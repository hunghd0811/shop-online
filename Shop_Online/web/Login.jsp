<%-- 
    Document   : Login
    Created on : Feb 27, 2022, 2:17:27 PM
    Author     : Black
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link href="style_1.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="center">
            <h1>Login</h1>
            <form action="login"method="post">
                <br>
                <p class="text-alert">${mess}</p>
                <div class="txt_field">
                    <input name="user" type="text" required>
                    <span></span>
                    <label>Username</label>
                </div>
                <div class="txt_field">
                    <input name="pass" type="password" required>
                    <span></span>
                    <label>Password</label>
                </div>
                <!--<div class="pass">Forgot Password?</div>-->
                <div class="pass">
                    <input name="remember" type="checkbox" id="Remember">
                    <label for="Remember" >Remember me</label>
                </div>
                <input type="submit" value="Login">
                <div class="signup_link">
                    Not a member? <a href="SINGUP.jsp">Signup</a>
                </div>
            </form>
        </div>

    </body>
</html>

