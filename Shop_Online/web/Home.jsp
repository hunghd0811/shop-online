<%-- 
    Document   : Home
    Created on : Feb 27, 2022, 1:49:01 PM
    Author     : Black
--%>

<%@include file="Menu.jsp" %>
        <!-- Start Product Area -->
        <div class="product-area section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h2>Buy Buy</h2>
                        </div>
                    </div>
                </div>
                <div class="row-10">
                    <div class="col-40">
                        <div class="product-info">
                            <div class="nav-main">
                                <!-- Tab Nav -->
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item active"><a class="nav-link" href="home">All product</a></li>
                                        <c:forEach items="${listC}" var="o">   
                                        <li class="nav-item active"><a class="nav-link" href="category?id=${o.categoryid}">${o.categoryname}</a></li>
                                        </c:forEach>
                                </ul>
                                <!--/ End Tab Nav -->
                            </div>
                            <div class="tab-content" id="myTabContent">
                                <!-- Start Single Tab -->
                                <div class="tab-pane fade show active" id="man" role="tabpanel">
                                    <div class="tab-single">
                                        <div id="content" class="row">
                                            <c:forEach items="${listP}" var="o" >
                                                <div class="col-xl-4 col-lg-5 col-md-5 col-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="detail?productid=${o.id}">
                                                                <img class="default-img" src="${o.image}" alt="#">
                                                            </a>
                                                            <div class="button-head">
                                                                <div class="product-action">
                                                                    <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
                                                                    <a title="Wishlist" href="#"><i class=" ti-heart "></i><span>Add to Wishlist</span></a>
                                                                    <a title="Compare" href="#"><i class="ti-bar-chart-alt"></i><span>Add to Compare</span></a>
                                                                </div>
                                                                <div class="product-action-2">
                                                                    <a title="Add to cart" href="add-to-cart?productid=${o.id}">Add to cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-content">
                                                            <h3><a href="detail?productid=${o.id}">${o.name}</a></h3>
                                                            <div class="product-price">
                                                                <span>$${o.price}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach> 
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" role="tabpanel">
                                    <div class="tab-single">
                                        <div class="row">
                                            <c:forEach  items="${listP}" var="o">
                                                <div class="col-xl-3 col-lg-4 col-md-4 col-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="product-details.html">
                                                                <img class="default-img" src="${o.image}" alt="#">
                                                            </a>
                                                            <div class="button-head">
                                                                <div class="product-action">
                                                                    <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
                                                                    <a title="Wishlist" href="#"><i class=" ti-heart "></i><span>Add to Wishlist</span></a>
                                                                    <a title="Compare" href="#"><i class="ti-bar-chart-alt"></i><span>Add to Compare</span></a>
                                                                </div>
                                                                <div class="product-action-2">
                                                                    <a title="Add to cart" href="add-to-cart?productid=${o.id}">Add to cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-content">
                                                            <h3><a href="product-details.html">${o.name}</a></h3>
                                                            <div class="product-price">
                                                                <span>$${o.price}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End Product Area -->


                                        <%@include file="Footer.jsp" %>
                                        
