<%-- 
    Document   : Detail
    Created on : Mar 4, 2022, 3:10:56 AM
    Author     : Black
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <title>Home</title>
        <!-- Favicon -->
        <link rel="icon" href="images/cat.jpg">
        <!-- Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Magnific Popup -->
        <!--<link rel="stylesheet" href="css/magnific-popup.min.css">-->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.css">
        <!-- Fancybox -->
        <!--<link rel="stylesheet" href="css/jquery.fancybox.min.css">-->
        <!-- Themify Icons -->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="css/niceselect.css">
        <!-- Animate CSS -->
        <!--<link rel="stylesheet" href="css/animate.css">-->
        <!-- Flex Slider CSS -->
        <link rel="stylesheet" href="css/flex-slider.min.css">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="css/owl-carousel.css">
        <!-- Slicknav -->
        <!--<link rel="stylesheet" href="css/slicknav.min.css">-->

        <!-- Eshop StyleSheet -->
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">



    </head>
    <body class="js">

        <!-- Preloader -->
        <!--        <div class="preloader">
                    <div class="preloader-inner">
                        <div class="preloader-icon">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>-->
        <!-- End Preloader -->


        <!-- Header -->
        <header class="header shop">
            <!-- Topbar -->
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 ml-auto col-12">
                            <div class="right-content">
                                <ul class="list-main">
                                    <c:if test="${sessionScope.acc.isAdmin == 1}">
                                        <li><i class="ti-anchor"></i> <a href="manager">Manager Account</a></li>                                           
                                        </c:if>
                                        <c:if test="${sessionScope.acc.isSell == 1}">
                                        <li><i class="ti-anchor"></i> <a href="manager">Manager Product</a></li>                                           
                                        </c:if>
                                        <c:if test="${sessionScope.acc != null}">
                                        <li><i class="ti-user"></i> <a href="#">My account</a></li>
                                        <li><i class="ti-power-off"></i><a href="logout">Logout</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.acc == null}">
                                        <li><i class="ti-power-off"></i><a href="Login.jsp">Login</a></li>
                                            </c:if>
                                </ul>
                            </div>
                        </div>
                        <!-- End Top Right -->
                    </div>
                </div>
            </div>
            <!-- Top Right -->

            <!-- End Topbar -->
            <div class="middle-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-12">
                            <!-- Logo -->
                            <div class="logo">
                                <a href="home"><img src="images/cat.jpg" alt="logo"></a>
                            </div>
                            <!--/ End Logo -->
                            <!-- Search Form -->
                            <div class="search-top">
                                <div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
                                <!-- Search Form -->
                                <div class="search-top">
                                    <form value="search" class="search-form">
                                        <input type="text" placeholder="Search here..." name="search">
                                        <button value="search" type="submit"><i class="ti-search"></i></button>
                                    </form>
                                </div>
                                <!--/ End Search Form -->
                            </div>
                            <!--/ End Search Form -->
                            <div class="mobile-nav"></div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-12">
                            <div class="search-bar-top">
                                <div class="search-bar">

                                    <form value="txtS" action="search" method="post">
                                        <input name="search" placeholder="Search Products Here....." type="search">
                                        <button class="btnn"><i class="ti-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-12">
                            <div class="right-bar">
                                <!-- Search Form -->
                                <div class="sinlge-bar">
                                    <a href="#" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="sinlge-bar">
                                    <a href="#" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="sinlge-bar shopping">
                                    <a href="#" class="single-icon"><i class="ti-bag"></i> <span class="total-count">${sessionScope.carts.size()}</span></a>
                                    <!-- Shopping Item -->
                                    <div class="shopping-item">
                                        <div class="dropdown-cart-header">
                                            <span>${sessionScope.carts.size()} Items</span>
                                            <a href="carts">View Cart</a>
                                        </div>
                                        
                                        <div class="bottom">
                                           
                                            <a href="checkout" class="btn animate">Checkout</a>
                                        </div>
                                    </div>
                                    <!--/ End Shopping Item -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header Inner -->
            <div class="header-inner">
                <div class="container">
                    <div class="cat-nav-head">
                        <div class="row">
                            <div class="col-lg-3">
<!--                                <div class="all-category">
                                    <h3 class="cat-heading"><i class="fa fa-bars active" aria-hidden="true"></i>Category </h3>
                                    <ul class="main-category">
                                        <c:forEach items="${listC}" var="o">   

                                            <li><a href="category?id=${o.categoryid}">${o.categoryname}</a></li>

                                        </c:forEach> 
                                    </ul>
                                </div>-->
                            </div>
                            <div class="col-lg-9 col-12">
                                <div class="menu-area">
                                    <!-- Main Menu -->
                                    <nav class="navbar navbar-expand-lg">
                                        <div class="navbar-collapse">	
                                            <div class="nav-inner">	
                                                <ul class="nav main-menu menu navbar-nav">
                                                    <li class="active"><a href="/Project_PRJ301_1/home">Home</a></li>
                                                    <li><a href="#">Product</a></li>												
                                                    <li><a href="#">Service</a></li>
                                                    <li><a href="#">Shop<i class="ti-angle-down"></i></a>
                                                        <ul class="dropdown">
                                                            <li><a href="carts">Cart</a></li>
                                                            <li><a href="checkout">Checkout</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Pages</a></li>									
                                                    <li><a href="contact.html">Contact Us</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </nav>
                                    <!--/ End Main Menu -->	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Inner -->
        </header>

        <!-- Breadcrumbs -->
        <div>

        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-20">
                        <div class="bread-inner">
                            <ul class="bread-list">
                                <li><a href="home">Home<i class="ti-arrow-right"></i></a></li>
                                <li class="active"><a href=detail">Detail</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrumbs -->

        <!-- Start Blog Single -->
        <section class="blog-single section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-15 col-20">
                        <div class="blog-single-main">
                            <div class="row">
                                <div class="col-20">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="#">
                                                <img class="product-img" src="${detail.image}" alt="#">
                                            </a>

                                            <div class="button-head">
                                                <div class="product-action">
                                                    <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
                                                    <a title="Wishlist" href="#"><i class=" ti-heart"></i><span>Add to Wishlist</span></a>
                                                    <a title="Compare" href="#"><i class="ti-bar-chart-alt"></i><span>Add to Compare</span></a>
                                                </div>
                                                <div class="product-action-2">
                                                    <a title="Add to cart" href="add-to-cart?productid=${detail.id}">Add to cart</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <h3><a href="product-details.html">${detail.name}</a></h3>
                                            <div class="product-price">
                                                <span>$${detail.price}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blog-detail">
                                        <h2 class="blog-title">${detail.name}</h2>

                                        <div class="content">
                                            <p>${detail.description}</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>         <!--/ End Single Widget -->
                <!-- Single Widget -->

            </div>
        </div>
    </div>
</div>
</section>
<!--/ End Blog Single -->

<!-- Start Footer Area -->
<!--<footer class="footer">
     Footer Top 
    <div class="footer-top section">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-12">
                     Single Widget 
                    <div class="single-footer about">
                        <div class="logo">
                            <a href="index.html"><img src="images/logo2.png" alt="#"></a>
                        </div>
                        <p class="text">Praesent dapibus, neque id cursus ucibus, tortor neque egestas augue,  magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
                        <p class="call">Got Question? Call us 24/7<span><a href="tel:123456789">+0123 456 789</a></span></p>
                    </div>
                     End Single Widget 
                </div>
                <div class="col-lg-2 col-md-6 col-12">
                     Single Widget 
                    <div class="single-footer links">
                        <h4>Information</h4>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Faq</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Help</a></li>
                        </ul>
                    </div>
                     End Single Widget 
                </div>
                <div class="col-lg-2 col-md-6 col-12">
                     Single Widget 
                    <div class="single-footer links">
                        <h4>Customer Service</h4>
                        <ul>
                            <li><a href="#">Payment Methods</a></li>
                            <li><a href="#">Money-back</a></li>
                            <li><a href="#">Returns</a></li>
                            <li><a href="#">Shipping</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                     End Single Widget 
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                     Single Widget 
                    <div class="single-footer social">
                        <h4>Get In Tuch</h4>
                         Single Widget 
                        <div class="contact">
                            <ul>
                                <li>NO. 342 - London Oxford Street.</li>
                                <li>012 United Kingdom.</li>
                                <li>info@eshop.com</li>
                                <li>+032 3456 7890</li>
                            </ul>
                        </div>
                         End Single Widget 
                        <ul>
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-flickr"></i></a></li>
                            <li><a href="#"><i class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                     End Single Widget 
                </div>
            </div>
        </div>
    </div>
     End Footer Top 
    <div class="copyright">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <div class="left">
                            <p>Copyright © 2020 <a href="http://www.wpthemesgrid.com" target="_blank">Wpthemesgrid</a>  -  All Rights Reserved.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="right">
                            <img src="images/payments.png" alt="#">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
 /End Footer Area 

 Jquery 
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.0.js"></script>
<script src="js/jquery-ui.min.js"></script>
 Popper JS 
<script src="js/popper.min.js"></script>
 Bootstrap JS 
<script src="js/bootstrap.min.js"></script>
 Color JS 
<script src="js/colors.js"></script>
 Slicknav JS 
<script src="js/slicknav.min.js"></script>
 Owl Carousel JS 
<script src="js/owl-carousel.js"></script>
 Magnific Popup JS 
<script src="js/magnific-popup.js"></script>
 Fancybox JS 
<script src="js/facnybox.min.js"></script>
 Waypoints JS 
<script src="js/waypoints.min.js"></script>
 Countdown JS 
<script src="js/finalcountdown.min.js"></script>
 Nice Select JS 
<script src="js/nicesellect.js"></script>
 Ytplayer JS 
<script src="js/ytplayer.min.js"></script>
 Flex Slider JS 
<script src="js/flex-slider.js"></script>
 ScrollUp JS 
<script src="js/scrollup.js"></script>
 Onepage Nav JS 
<script src="js/onepage-nav.min.js"></script>
 Easing JS 
<script src="js/easing.js"></script>
 Active JS 
<script src="js/active.js"></script>
</body>
</html>-->
<%@include file="Footer.jsp" %>
