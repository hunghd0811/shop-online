<%-- 
    Document   : songDetail
    Created on : Jun 5, 2022, 6:05:16 PM
    Author     : Auriat
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="sliders" scope="page" value="${requestScope.sliders}" />
<c:set var="albums_Vpop" scope="page" value="${requestScope.albums_Vpop}" />
<c:set var="albums_USUK" scope="page" value="${requestScope.albums_USUK}" />
<c:set var="albums_Lofi" scope="page" value="${requestScope.albums_Lofi}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Song</title>
        <link rel="stylesheet" href="./css/app.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
        <meta charset="utf-8">
        <!--  This file has been downloaded from bootdey.com @bootdey on twitter -->
        <!--  All snippets are MIT license http://bootdey.com/license -->

        <title>Loustic</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <style>
            @media all and (min-width: 992px) {
                .header__nav .nav-item .dropdown-menu{ display: none; }
                .header__nav .nav-item:hover .nav-link{   }
                .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
                .header__nav .nav-item .dropdown-menu{ margin-top:0; }
            }
            .slider__item .slider__content {
                margin-bottom: 500px;
            }
            .bg-overlay::after {
                position: absolute;
                content: "";
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
                z-index: -1;
            }

            [id="heart"] {
                position: absolute;
                left: -100vw;
            }

            [for="heart"] {
                color: #aab8c2;
                cursor: pointer;
                font-size: 17px;
                align-self: center;  
                transition: color 0.2s ease-in-out;
            }

            [for="heart"]:hover {
                color: grey;
            }

            [for="heart"]::selection {
                color: none;
                background: transparent;
            }

            [for="heart"]::moz-selection {
                color: none;
                background: transparent;
            }


            [id="heart"]:checked + label {
                color: #e2264d;
                will-change: font-size;
                animation: heart 1s cubic-bezier(.17, .89, .32, 1.49);
            }

            .lyric.active {
                color: yellow;
                font-size: 20px;
            }

            @keyframes heart {0%, 17.5% {font-size: 0;}}
            select{
                background-color: #616161;
                color: white;
                border-radius: 10px;
            }
        </style>
    </head>

    <body>
        <%--Hearder--%>
        <section id="header" style="background: #f1b0b7">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>

                    <%--Search--%>
                    <div class="header__search"> 
                        <form value="txtSearch" action="search" method="post" style="display : flex;">
                            <input type="text" name="search" placeholder="Search and hit enter..." style="margin-top: 10px">
                            <select name="searchType" style="height: 30px; margin-top: 12px;">
                                <option value="1">Search By Name</option>
                                <option value="2">Search By Lyrics</option>
                                <option value="3">Search By Author</option>

                            </select>
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content" style="background-color: #f1b0b7"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="./listacc">Manage Account </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./manageAlbum">Manage Albums</a></li>
                                        <li><a class="dropdown-item" href="./manager">Manage Songs</a></li>
                                        <li><a class="dropdown-item" href="./managecomment">Manage Comment</a></li>
                                        <li><a class="dropdown-item" href="managehistory">Manage View History</a></li>
                                    </ul>
                                </li>
                            </c:if>
                            <c:if test="${user.role_ID == 3}">
                                <li><a href="./manager">Manage Songs</a></li>
                                </c:if>    
                            <li><a  href="./home">Home </a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${1}">Nhạc Việt Nam </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${1}"> Nhạc Trẻ</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${2}"> Nhạc Trữ Tình </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${3}"> Nhạc Cách Mạng </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${2}">Nhạc Quốc Tế </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${4}"> Nhạc US-UK</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${5}"> Nhạc KPOP </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${6}"> Nhạc Anime </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${3}">Lofi </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${8}"> Nhạc Piano</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${9}"> Nhạc Guitar </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${7}"> Nhạc Chill </a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link" href="./albums">Albums </a>

                                <c:if test="${user != null}">
                                <li><a href="./allplaylist">View PlayList </a></li>                                
                                </c:if>
                            </li>
                            <c:if test="${user != null}">
                                <li><a href="history">Recently Visited</a></li>
                                </c:if>

                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post" style="display: flex">
                                <input type="text" name="txtsearch" placeholder="Search and hit enter..." style="margin-top: 10px">
                                <select name="searchType" style="height: 30px; margin-top: 12px">
                                    <option value="1">Search By Name</option>
                                    <option value="2">Search By Lyrics</option>
                                    <option value="3">Search By Author</option>

                                </select>
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social">      
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>

                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" 
                                                                     <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                         style="background-image: url('./img/avatar.jpg');"
                                                                     </c:if>
                                                                     <c:if test="${user.avatar != null}">
                                                                         style="background-image: url('./img/uploads/${user.avatar}');"
                                                                     </c:if>
                                                                     ></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section>      
        <section id="slider">
            <div class="slider__item bg-overlay bg-img slider__item-1 active" style="background-image: url('${requestScope.slider.image}');">
                <div class="container" style="margin-top: 150px;">
                    <div class="slider__content">
                        <div class="slider__wellcome"> 
                            <div class="slider__group--btn">
                                <form>
                                    <input type="button" class="btn filter" value="Back" onclick="history.back()">
                                </form>
                            </div>
                        </div>
                        <div class="slider__player player song-item" data-path="${requestScope.slider.path}" style="margin-top: -170px;">
                            <!--                            max-width: 90%; max-height: 150px"-->
                            <div class="player__img bg-img" style="background-image: url('${requestScope.slider.image}');">
                                <div class="song-img--hover"></div>
                            </div>
                            <div class="player__content">
                                <div class="player__info">
                                    <p class="player__date">${requestScope.slider.t_create}</p>
                                    <h1 class="player__name song-name">${requestScope.slider.name}</h1>
                                    <p class="player__text"><span class="player__author">${requestScope.slider.author} | </span><span class="player_duration">00:${requestScope.slider.duration}</span></p>
                                </div>
                                <div class="player__control">
                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                    <div class="sidebarTime--bg">
                                        <div class="sidebarTime--current"> </div>
                                    </div><span class="duration">${requestScope.slider.duration}</span>
                                    <div class="volume">
                                        <div class="volume__icon"> <i class="mute fas fa-volume-slash"></i><i class="volume--low fas fa-volume active"></i><i class="volume--hight fas fa-volume-up"></i></div>
                                    </div>
                                    <div class="volume__silebar--bg">
                                        <div class="volume__silebar--current"></div>
                                    </div>
                                </div>
                                <div class="like-share-download">
                                    <div class="option like">

                                        <form action="manageLike" method="POST">

                                            <input id="heart" type="checkbox" name="like" onClick="this.form.submit()" ${requestScope.checkerviet == true?"checked":""}/>
                                            <label for="heart">❤</label>
                                            <span>Like</span>

                                            <input value="${requestScope.slider.song_ID}" hidden="true" name="songID" />
                                        </form>
                                        Total Views: ${TotalViews}<br/>
                                        <c:if test="${user != null}">
                                            <c:if test="${userTotalViews < 2}">
                                                You have visited this song ${userTotalViews} time
                                            </c:if>
                                            <c:if test="${userTotalViews > 1}">
                                                You have visited this song ${userTotalViews} times
                                            </c:if>
                                        </c:if>
                                    </div>
                                    <div class="div">
                                        <div class="option share"> <i class="fas fa-share-alt"> <span>Share</span></i></div>
                                        <div class="option download" ><c:if test="${user != null}"><a href="${requestScope.slider.path}" style="color: black" download></c:if><i class="fas fa-download"> <span>Download</span></i></a></div>
                                        <c:if test="${user != null}">
                                            <c:if test="${userLastVisited != null}">
                                                <br/><br/>Last visited: ${userLastVisited}
                                            </c:if>
                                            <c:if test="${userLastVisited == null}">
                                                <br/><br/>Last visited: Now
                                            </c:if>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="lyrics" style="margin:auto; color: white">
                            <c:forEach var="i" begin="0" end="${lyrics.size()}">
                                <p class="lyric ${i == 0 ? "active" : ""}" data-time="${lyrics[i].time}">${lyrics[i].content}</p>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<section class="content-item" id="comments">
    <div class="container">   
        <div class="row align-items-center">
            <div class="col-sm-8">   
                <form action="addcomment" method="get">
                    <h3 class="pull-left">New Comment</h3>

                    <fieldset>
                        <input type="hidden" name="id" value="${requestScope.slider.song_ID}">
                        <div class="row">
                            <div class="col-sm-3 col-lg-2 hidden-xs">
                                <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                    <img class="img-responsive" src="./img/avatar.jpg" alt="">
                                </c:if>
                                <c:if test="${user.avatar != null}">
                                    <img class="img-responsive" src="./img/uploads/${user.avatar}" alt="">
                                </c:if>
                            </div>
                            <div class="form-group col-xs-12 col-sm-9 col-lg-10">
                                <textarea class="form-control" id="message" name="comment" placeholder="Your Comment" required=""></textarea>                                                  

                            </div>
                        </div>  	
                    </fieldset>
                    <button type="submit" class="btn btn-normal pull-right">Submit</button>
                </form>



                <c:forEach items="${requestScope.cmm}" var="cm">
                    <c:if test="${cm.status == true}">

                        <div class="media">
                            <a class="pull-left" href="#"><img class="media-object" src="./img/avatar.jpg" alt=""></a>
                            <div class="media-body">
                                <c:forEach items="${requestScope.user1}" var="a">
                                    <c:if test="${a.user_ID == cm.userid}">
                                        <p class="media-heading">${a.last_name}</p>
                                    </c:if>
                                </c:forEach>
                                <form id="myForm" action="edicomment" method="get">
                                    <input name="comment" class="" value="${cm.comment}" type="text">
                                    <input value="${cm.commentid}" hidden="true" name="commentID" />
                                    <input value="${requestScope.slider.song_ID}" hidden="true" name="songID" />
                                    <input value="${cm.userid}" hidden="true" name="userID" />
                                </form>                                <ul class="list-unstyled list-inline media-detail pull-left">
                                    <li><i class="fa fa-calendar"></i>${cm.tcreate}</li>
                                    <li><i class="fa fa-calendar"></i>${cm.tlastupdate}</li>
                                    <li><i class="fa fa-thumbs-up"></i>${cm.like}</li>
                                </ul>

                                <ul class="list-unstyled list-inline media-detail pull-right">

                                    <a class="fas fa-heart" 


                                       <c:forEach items="${requestScope.likeCheck}" var="l">

                                           <c:if test="${l.comment_ID == cm.commentid}" > style="color: red"  </c:if>

                                       </c:forEach>


                                       ></a>


                                    <li class=""><a href="manageLike?commentID=${cm.commentid}&songID=${cm.songid}&flag=&userid=${sessionScope.user.user_ID}&operation=likecomment">Like</a></li>
                                    <li class=""><a href="manageLike?commentID=${cm.commentid}&songID=${cm.songid}&flag=0&userid=${sessionScope.user.user_ID}&operation=likecomment">Unlike</a></li>
                                    <li class="" onclick="replyDisplay()" id="reply-${cm.commentid}">Reply</li>
                                    <li class=""><a onClick="myFunction()">Edit</a></li>
                                    <li class=""><form action="deletecomment" method="get">  
                                            <input type="hidden" name="commentid" value="${cm.commentid}">
                                            <input type="hidden" name="id" value="${cm.songid}">
                                            <button type="submit" value="Delete">Delete</button>
                                        </form>
                                    </li>
                                    <li class=""><a href="report?id=${cm.commentid}&songid=${cm.songid}">Report</a></li>

                                </ul>

                            </div>
                            <div style="padding-left: 10%">
                                <c:forEach items="${requestScope.reply}" var="rcm">
                                    <c:if test="${rcm.comment_ID == cm.commentid}">
                                        <div>
                                            <div class="media">
                                                <a class="pull-left" href="#"><img class="media-object" src="./img/avatar.jpg" alt=""></a>
                                                <div class="media-body">
                                                    <c:forEach items="${requestScope.user1}" var="a">
                                                        <c:if test="${a.user_ID == rcm.user_ID}">
                                                            <p class="media-heading">${a.last_name}</p>
                                                        </c:if>
                                                    </c:forEach>

                                                </div>
                                                <div>
                                                    <p>${rcm.content}</p>
                                                    <ul class="list-unstyled list-inline media-detail pull-left">
                                                        <li><i class="fa fa-calendar"></i>${rcm.t_create}</li>
                                                    </ul>
                                                    <ul class="list-unstyled list-inline media-detail pull-right">
                                                        <c:if test="${rcm.user_ID == sessionScope.user.user_ID}">
                                                            <li class=""><a href="reply?replyID=${rcm.id}&songID=${cm.songid}">Delete</a></li>
                                                            </c:if>
                                                        <li class=""><a href="report?id=${cm.commentid}&songid=${cm.songid}">Report</a></li>
                                                    </ul>

                                                </div>

                                            </div>

                                        </div>

                                    </c:if>

                                </c:forEach> 
                                <div id="replyform-${cm.commentid}">
                                    <form action="reply" method="POST" >
                                        <h3 class="pull-left">Reply Comment</h3>

                                        <fieldset>
                                            <input type="hidden" name="songID" value="${cm.songid}">
                                            <input type="hidden" name="commentID" value="${cm.commentid}">

                                            <div class="row">
                                                <div class="col-sm-3 col-lg-2 hidden-xs">
                                                    <img class="img-responsive" src="./img/avatar.jpg" alt="">
                                                </div>
                                                <div class="form-group col-xs-12 col-sm-9 col-lg-10">
                                                    <textarea class="form-control" name="content" placeholder="Your Comment" required=""></textarea>                                                  

                                                </div>
                                            </div>  	
                                        </fieldset>
                                        <button type="submit" class="btn btn-normal pull-right">Submit</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </c:if>
                </c:forEach>
                <!-- COMMENT 1 - END -->
            </div>
        </div>
    </div>
</section>
</div>

<style type="text/css">
    body{margin-top:20px;}

    .content-item {
        padding:30px 0;
        background-color:#FFFFFF;
    }

    .content-item.grey {
        background-color:#F0F0F0;
        padding:50px 0;
        height:100%;
    }

    .content-item h2 {
        font-weight:700;
        font-size:35px;
        line-height:45px;
        text-transform:uppercase;
        margin:20px 0;
    }

    .content-item h3 {
        font-weight:400;
        font-size:20px;
        color:#555555;
        margin:10px 0 15px;
        padding:0;
    }

    .content-headline {
        height:1px;
        text-align:center;
        margin:20px 0 70px;
    }

    .content-headline h2 {
        background-color:#FFFFFF;
        display:inline-block;
        margin:-20px auto 0;
        padding:0 20px;
    }

    .grey .content-headline h2 {
        background-color:#F0F0F0;
    }

    .content-headline h3 {
        font-size:14px;
        color:#AAAAAA;
        display:block;
    }


    #comments {
        box-shadow: 0 -1px 6px 1px rgba(0,0,0,0.1);
        background-color:#FFFFFF;
    }

    #comments form {
        margin-bottom:30px;
    }

    #comments .btn {
        margin-top:7px;
    }

    #comments form fieldset {
        clear:both;
    }

    #comments form textarea {
        height:100px;
    }

    #comments .media {
        border-top:1px dashed #DDDDDD;
        padding:20px 0;
        margin:0;
    }

    #comments .media > .pull-left {
        margin-right:20px;
    }

    #comments .media img {
        max-width:100px;
    }

    #comments .media h4 {
        margin:0 0 10px;
    }

    #comments .media h4 span {
        font-size:14px;
        float:right;
        color:#999999;
    }

    #comments .media p {
        margin-bottom:15px;
        text-align:justify;
    }

    #comments .media-detail {
        margin:0;
    }

    #comments .media-detail li {
        color:#AAAAAA;
        font-size:12px;
        padding-right: 10px;
        font-weight:600;
    }

    #comments .media-detail a:hover {
        text-decoration:underline;
    }

    #comments .media-detail li:last-child {
        padding-right:0;
    }

    #comments .media-detail li i {
        color:#666666;
        font-size:15px;
        margin-right:10px;
    }

    #footer .footer__container {
        display: flex;
    }
</style>
<script>
    function myFunction() {
        document.getElementById("myForm").submit();
    }
</script>


</div>
</div>
</section>

<section id="newletter">
    <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
        <div class="container newletter__container">
            <div class="newletter__content">
                <h1>Sign Up To Newsletter</h1>
                <p>Subscribe to receive info on our latest news and episodes</p>
            </div>

            <div class="newletter__subcribe">
                <form action="sendnew" method="get">
                    <input type="text" placeholder="Your Email" name="email">
                    <input type="submit" value="SUBCRIBE">
                </form>
            </div>
        </div>
    </div>
</section>
<section id="footer">
    <div class="container footer__container">
        <div class="footer__about"> 
            <h2>About Us</h2>
            <p>It is a long established fact that a reader will be distracted by the readable content.</p>
            <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
        </div>
        <ul class="footer__categories"> 
            <h2>Categories</h2>
            <li><a href="#" style="color: black">Entrepreneurship </a></li>
            <li><a href="#" style="color: black">Media </a></li>
            <li><a href="#" style="color: black">Tech </a></li>
            <li>   <a href="#" style="color: black">Tutorials </a></li>
        </ul>
        <div class="footer_social"> 
            <h2>Follow Us</h2>
            <ul class="media">
                <li><a class="fab fa-facebook" href="#" style="color: black"> </a></li>
                <li><a class="fab fa-twitter" href="#" style="color: black"> </a></li>
                <li><a class="fab fa-pinterest" href="#" style="color: black"> </a></li>
                <li><a class="fab fa-instagram" href="#" style="color: black"> </a></li>
                <li><a class="fab fa-youtube" href="#" style="color: black"> </a></li>
            </ul>
            <ul class="store"> 
                <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
            </ul>
        </div>
    </div>
</section>


<section id="toast"></section>
<audio id="audio" src="">    </audio>
<script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
<script src="./js/app.js"></script>
<script src="./js/jquery-3.2.1.min.js"></script>
<script src="./js/handle_ajax.js"></script>
<script src="./js/handle_toast.js"></script>
<script src="./js/lyric.js"></script>
</body>
</html>
