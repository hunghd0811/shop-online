<%-- 
    Document   : SINGUP
    Created on : Feb 28, 2022, 9:17:40 PM
    Author     : Black
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link href="style_1.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="center">
            <h1>Sign up</h1>
            <form action="signup"method="post">
                <br>
                <p class="text-danger">${mess}</p>
                <div class="txt_field">
                    <input name="email" type="text" required>
                    <span></span>
                    <label>Email</label>
                </div>
                <div class="txt_field">
                    <input name="user" type="text" required>
                    <span></span>
                    <label>Username</label>
                </div>
                <div class="txt_field">
                    <input name="pass" type="password" required>
                    <span></span>
                    <label>Password</label>
                </div>
                <div class="txt_field">
                    <input name="repass" type="password" required>
                    <span></span>
                    <label>Confirm Password</label>
                </div>
                <input type="submit" value="Signup">
                <div class="signup_link">
                    Already have an account? <a href="Login.jsp">Login</a>
                </div>
            </form>
        </div>

    </body>
</html>


