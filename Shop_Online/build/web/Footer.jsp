<%-- 
    Document   : Footer
    Created on : Mar 17, 2022, 11:52:25 AM
    Author     : Black
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
          <head>
<!--         Meta Tag 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name='copyright' content=''>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="images/favicon.png">
        <!-- Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

        <!-- StyleSheet -->

        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Magnific Popup -->
        <!--<link rel="stylesheet" href="css/magnific-popup.min.css">-->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.css">
        <!-- Fancybox -->
        <!--<link rel="stylesheet" href="css/jquery.fancybox.min.css">-->
        <!-- Themify Icons -->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="css/niceselect.css">
        <!-- Animate CSS -->
        <!--<link rel="stylesheet" href="css/animate.css">-->
        <!-- Flex Slider CSS -->
        <link rel="stylesheet" href="css/flex-slider.min.css">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="css/owl-carousel.css">
        <!-- Slicknav -->
        <!--<link rel="stylesheet" href="css/slicknav.min.css">-->

        <!-- Eshop StyleSheet -->
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">



    </head>
    </head>
    <body>
        <footer class="footer">
                                            <!-- Footer Top -->
                                            <div class="footer-top section">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-md-6 col-12">
                                                            <!-- Single Widget -->
                                                            <div class="single-footer about">
                                                                <div class="logo">
                                                                    <a href="home"><img src="images/cat.jpg" alt="#"></a>
                                                                </div>
                                                                <p class="text">Praesent dapibus, neque id cursus ucibus, tortor neque egestas augue,  magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
                                                                <p class="call">Got Question? Call us 24/7<span><a href="tel:0385999716">0385999716</a></span></p>
                                                            </div>
                                                            <!-- End Single Widget -->
                                                        </div>
                                                        <div class="col-lg-2 col-md-6 col-12">
                                                            <!-- Single Widget -->
                                                            <div class="single-footer links">
                                                                <h4>Information</h4>
                                                                <ul>
                                                                    <li><a href="#">About Us</a></li>
                                                                    <li><a href="#">Faq</a></li>
                                                                    <li><a href="#">Terms & Conditions</a></li>
                                                                    <li><a href="#">Contact Us</a></li>
                                                                    <li><a href="#">Help</a></li>
                                                                </ul>
                                                            </div>
                                                            <!-- End Single Widget -->
                                                        </div>
                                                        <div class="col-lg-2 col-md-6 col-12">
                                                            <!-- Single Widget -->
                                                            <div class="single-footer links">
                                                                <h4>Customer Service</h4>
                                                                <ul>
                                                                    <li><a href="#">Payment Methods</a></li>
                                                                    <li><a href="#">Money-back</a></li>
                                                                    <li><a href="#">Returns</a></li>
                                                                    <li><a href="#">Shipping</a></li>
                                                                    <li><a href="#">Privacy Policy</a></li>
                                                                </ul>
                                                            </div>
                                                            <!-- End Single Widget -->
                                                        </div>
                                                        <div class="col-lg-3 col-md-6 col-12">
                                                            <!-- Single Widget -->
                                                            <div class="single-footer social">
                                                                <h4>Get In Tuch</h4>
                                                                <!-- Single Widget -->
                                                                <div class="contact">
                                                                    <ul>
                                                                        <li>KM29</li>
                                                                        <li></li>
                                                                        <li>+0385999716</li>
                                                                    </ul>
                                                                </div>
                                                                <!-- End Single Widget -->
                                                                <ul>
                                                                    <li><a href="#"><i class="ti-facebook"></i></a></li>
                                                                    <li><a href="#"><i class="ti-twitter"></i></a></li>
                                                                    <li><a href="#"><i class="ti-flickr"></i></a></li>
                                                                    <li><a href="#"><i class="ti-instagram"></i></a></li>
                                                                </ul>
                                                            </div>
                                                            <!-- End Single Widget -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Footer Top -->
                                            <div class="copyright">
                                                <div class="container">
                                                    <div class="inner">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-12">
                                                                <div class="left">
                                                                    <p>Copyright © 2022 <a href="http://www.wpthemesgrid.com" target="_blank">Wpthemesgrid</a>  -  All Rights Reserved.</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-12">
                                                                <div class="right">
                                                                    <img src="images/payments.png" alt="#">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </footer>
                                        <!-- /End Footer Area -->
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

                                        <script src="js/jquery.min.js"></script>
                                        <script src="js/jquery-migrate-3.0.0.js"></script>
                                        <script src="js/jquery-ui.min.js"></script>                                    
                                        <script src="js/popper.min.js"></script>                                  
                                        <script src="js/bootstrap.min.js"></script>                                   
                                        <script src="js/colors.js"></script>                                 
                                        <script src="js/slicknav.min.js"></script>
                                        <script src="js/owl-carousel.js"></script>
                                        <script src="js/magnific-popup.js"></script>
                                        <script src="js/waypoints.min.js"></script>
                                        <script src="js/finalcountdown.min.js"></script>
                                        <script src="js/nicesellect.js"></script>
                                        <script src="js/flex-slider.js"></script>
                                        <script src="js/scrollup.js"></script>
                                        <script src="js/onepage-nav.min.js"></script>
                                        <script src="js/easing.js"></script>
                                        <script src="js/active.js"></script>
                                        </body>
                                        </html>
