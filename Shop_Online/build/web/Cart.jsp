<%-- 
    Document   : Cart
    Created on : Mar 14, 2022, 5:34:07 AM
    Author     : Black
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="icon" href="images/cat.jpg">
        <!-- Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

        <!-- StyleSheet -->

        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Magnific Popup -->
        <!--<link rel="stylesheet" href="css/magnific-popup.min.css">-->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.css">
        <!-- Fancybox -->
        <!--<link rel="stylesheet" href="css/jquery.fancybox.min.css">-->
        <!-- Themify Icons -->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="css/niceselect.css">
        <!-- Animate CSS -->
        <!--<link rel="stylesheet" href="css/animate.css">-->
        <!-- Flex Slider CSS -->
        <link rel="stylesheet" href="css/flex-slider.min.css">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="css/owl-carousel.css">
        <!-- Slicknav -->
        <!--<link rel="stylesheet" href="css/slicknav.min.css">-->

        <!-- Eshop StyleSheet -->
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">



    </head>
    <body class="js">
        <!-- Header -->
        <header class="header shop">
            <!-- Topbar -->
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 ml-auto col-12">
                            <div class="right-content">
                                <ul class="list-main">
                                    <c:if test="${sessionScope.acc.isAdmin == 1}">
                                        <li><i class="ti-anchor"></i> <a href="manager">Manager Account</a></li>                                           
                                        </c:if>
                                        <c:if test="${sessionScope.acc.isSell == 1}">
                                        <li><i class="ti-anchor"></i> <a href="manager">Manager Product</a></li>                                           
                                        </c:if>
                                        <c:if test="${sessionScope.acc != null}">
                                        <li><i class="ti-user"></i> <a href="#">My account</a></li>
                                        <li><i class="ti-power-off"></i><a href="logout">Logout</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.acc == null}">
                                        <li><i class="ti-power-off"></i><a href="Login.jsp">Login</a></li>
                                            </c:if>
                                </ul>
                            </div>
                        </div>
                        <!-- End Top Right -->
                    </div>
                </div>
            </div>
            <!-- Top Right -->

            <!-- End Topbar -->
            <div class="middle-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-12">
                            <!-- Logo -->
                            <div class="logo">
                                <a href="home"><img src="images/cat.jpg" alt="logo"></a>
                            </div>
                            <!--/ End Logo -->
                            <!-- Search Form -->
                            <div class="search-top">
                                <div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
                                <!-- Search Form -->
                                <div class="search-top">
                                    <form value="search" class="search-form">
                                        <input type="text" placeholder="Search here..." name="search">
                                        <button value="search" type="submit"><i class="ti-search"></i></button>
                                    </form>
                                </div>
                                <!--/ End Search Form -->
                            </div>
                            <!--/ End Search Form -->
                            <div class="mobile-nav"></div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-12">
                            <div class="search-bar-top">
                                <div class="search-bar">

                                    <form value="txtS" action="search" method="post">
                                        <input name="search" placeholder="Search Products Here....." type="search">
                                        <button class="btnn"><i class="ti-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-12">
                            <div class="right-bar">
                                <!-- Search Form -->
                                <div class="sinlge-bar">
                                    <a href="#" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="sinlge-bar">
                                    <a href="#" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="sinlge-bar shopping">
                                    <a href="#" class="single-icon"><i class="ti-bag"></i> <span class="total-count">${sessionScope.carts.size()}</span></a>
                                    <!-- Shopping Item -->
                                    <div class="shopping-item">
                                        <div class="dropdown-cart-header">
                                            <span>${sessionScope.carts.size()} Items</span>
                                            <a href="carts">View Cart</a>
                                        </div>
                                        
                                        <div class="bottom">
                                           
                                            <a href="checkout" class="btn animate">Checkout</a>
                                        </div>
                                    </div>
                                    <!--/ End Shopping Item -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header Inner -->
            <div class="header-inner">
                <div class="container">
                    <div class="cat-nav-head">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="all-category">
                                    <h3 class="cat-heading"><i class="fa fa-bars active" aria-hidden="true"></i>Category </h3>
                                    <ul class="main-category">
                                        <c:forEach items="${listC}" var="o">   

                                            <li><a href="category?id=${o.categoryid}">${o.categoryname}</a></li>

                                        </c:forEach> 
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-9 col-12">
                                <div class="menu-area">
                                    <!-- Main Menu -->
                                    <nav class="navbar navbar-expand-lg">
                                        <div class="navbar-collapse">	
                                            <div class="nav-inner">	
                                                <ul class="nav main-menu menu navbar-nav">
                                                    <li class="active"><a href="/Project_PRJ301_1/home">Home</a></li>
                                                    <li><a href="#">Product</a></li>												
                                                    <li><a href="#">Service</a></li>
                                                    <li><a href="#">Shop<i class="ti-angle-down"></i></a>
                                                        <ul class="dropdown">
                                                            <li><a href="carts">Cart</a></li>
                                                            <li><a href="checkout">Checkout</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Pages</a></li>									
                                                    <li><a href="contact.html">Contact Us</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </nav>
                                    <!--/ End Main Menu -->	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Inner -->
        </header>
        <!--/ End Header -->

       
        <!--/ End Slider Area -->
  


        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="bread-inner">
                            <ul class="bread-list">
                                <li><a href="/Project_PRJ301_1/home">Home<i class="ti-arrow-right"></i></a></li>
                                <li class="active"><a href="/Project_PRJ301_1/carts">Cart</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrumbs -->

        <!-- Shopping Cart -->
        <div class="shopping-cart section">
            <div class="container" style="min-height: 700px">
                <div class="row">
                    <div class="col-12">
                        <!-- Shopping Summery -->
                        <c:choose>
                            <c:when test="${sessionScope.carts==null||sessionScope.carts.size()==0}">
                                <div class="col-12 text-center mt-5">
                                    <h3>List Cart Is Empty</h3>
                                    <br>
                                    <div class="button5">
                                        <a href="/Project_PRJ301_1/home" class="btn">Click to return home page</a>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <table class="table shopping-summery">
                                    <thead>
                                        <tr class="main-hading">
                                            <th class="text-center">ID</th>
                                            <th class="text-center">PRODUCT</th>
                                            <th class="text-center">NAME</th>
                                            <th class="text-center">UNIT PRICE</th>
                                            <th class="text-center">QUANTITY</th>
                                            <th class="text-center">TOTAL PRICE</th> 
                                            <th class="text-center">ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${carts}" var="o" >
                                        <form action="update-quantity">
                                            <tr>
                                            <input  type="hidden" name="productid" value="${o.value.product.id}">
                                            <td class="product-des" title="ProductID">
                                                <p class="product-name"><a href="#">${o.value.product.id}</a></p>
                                            </td>
                                            <td class="image" title="Image"><img src="${o.value.product.image}"  alt="#"></td>
                                            <td class="product-des" title="Name">
                                                <p class="product-name"><a href="#">${o.value.product.name}</a></p>
                                            </td>
                                            <td class="price" title="Price"><span>$${o.value.product.price} </span></td>     
                                            <td class="quantity buttons_added">
                                                <input onchange="this.form.submit()" type="number" step="1" min="1" max="" name="quantity" value="${o.value.quantity}" title="Quantity" class="input-text qty text" size="4" pattern="" inputmode="">
                                            </td>
                                            <td class="total-amount" title="Total"><span>$${o.value.product.price*o.value.quantity}</span></td>
                                            <td class="action" title="Remove"><a href="delete-cart?productid=${o.value.product.id}"><i class="ti-trash"></i>Delete</a></td>
                                            </tr>
                                        </form>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${sessionScope.carts==null||sessionScope.carts.size()==0}">
                        <h1></h1>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div class="col-12">
                                <!-- Total Amount -->
                                <div class="total-amount">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-5 col-12">
                                            <div class="left">
                                                <div class="coupon">

                                                </div>
                                                <div class="checkbox">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-7 col-12">
                                            <div class="right">
                                                <ul>
                                                    <li>Cart Subtotal<span>$${totalMoney}</span></li>
                                                </ul>
                                                <div class="button5">
                                                    <a href="checkout" class="btn">Checkout</a>
                                                    <a href="/Project_PRJ301_1/home" class="btn">Continue shopping</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ End Total Amount -->
                            </div>
                        </div>

                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <!--/ End Shopping Cart -->





        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row no-gutters">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <!-- Product Slider -->
                                <div class="product-gallery">
                                    <div class="quickview-slider-active">
                                        <div class="single-slider">
                                            <img src="images/modal1.jpg" alt="#">
                                        </div>
                                        <div class="single-slider">
                                            <img src="images/modal2.jpg" alt="#">
                                        </div>
                                        <div class="single-slider">
                                            <img src="images/modal3.jpg" alt="#">
                                        </div>
                                        <div class="single-slider">
                                            <img src="images/modal4.jpg" alt="#">
                                        </div>
                                    </div>
                                </div>
                                <!-- End Product slider -->
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="quickview-content">
                                    <h2>Flared Shift Dress</h2>
                                    <div class="quickview-ratting-review">
                                        <div class="quickview-ratting-wrap">
                                            <div class="quickview-ratting">
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <a href="#"> (1 customer review)</a>
                                        </div>
                                        <div class="quickview-stock">
                                            <span><i class="fa fa-check-circle-o"></i> in stock</span>
                                        </div>
                                    </div>
                                    <h3>$29.00</h3>
                                    <div class="quickview-peragraph">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam.</p>
                                    </div>
                                    <div class="size">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <h5 class="title">Size</h5>
                                                <select>
                                                    <option selected="selected">s</option>
                                                    <option>m</option>
                                                    <option>l</option>
                                                    <option>xl</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <h5 class="title">Color</h5>
                                                <select>
                                                    <option selected="selected">orange</option>
                                                    <option>purple</option>
                                                    <option>black</option>
                                                    <option>pink</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="quantity">
                                        <!-- Input Order -->
                                        <div class="input-group">
                                            <div class="button minus">
                                                <button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                    <i class="ti-minus"></i>
                                                </button>
                                            </div>
                                            <input type="text" name="quant[1]" class="input-number"  data-min="1" data-max="1000" value="1">
                                            <div class="button plus">
                                                <button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[1]">
                                                    <i class="ti-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <!--/ End Input Order -->
                                    </div>
                                    <div class="add-to-cart">
                                        <a href="#" class="btn">Add to cart</a>
                                        <a href="#" class="btn min"><i class="ti-heart"></i></a>
                                        <a href="#" class="btn min"><i class="fa fa-compress"></i></a>
                                    </div>
                                    <div class="default-social">
                                        <h4 class="share-now">Share:</h4>
                                        <ul>
                                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal end -->
<%@include file="Footer.jsp" %>
