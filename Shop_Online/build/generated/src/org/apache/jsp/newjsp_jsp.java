package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class newjsp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("\t<!-- Meta Tag -->\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("\t<meta name='copyright' content=''>\n");
      out.write("\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("\t<!-- Title Tag  -->\n");
      out.write("    <title>Eshop - eCommerce HTML5 Template.</title>\n");
      out.write("\t<!-- Favicon -->\n");
      out.write("\t<link rel=\"icon\" type=\"image/png\" href=\"images/favicon.png\">\n");
      out.write("\t<!-- Web Font -->\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap\" rel=\"stylesheet\">\n");
      out.write("\t\n");
      out.write("\t<!-- StyleSheet -->\n");
      out.write("\t\n");
      out.write("\t<!-- Bootstrap -->\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/bootstrap.css\">\n");
      out.write("\t<!-- Magnific Popup -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/magnific-popup.min.css\">\n");
      out.write("\t<!-- Font Awesome -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/font-awesome.css\">\n");
      out.write("\t<!-- Fancybox -->\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/jquery.fancybox.min.css\">\n");
      out.write("\t<!-- Themify Icons -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/themify-icons.css\">\n");
      out.write("\t<!-- Nice Select CSS -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/niceselect.css\">\n");
      out.write("\t<!-- Animate CSS -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/animate.css\">\n");
      out.write("\t<!-- Flex Slider CSS -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/flex-slider.min.css\">\n");
      out.write("\t<!-- Owl Carousel -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/owl-carousel.css\">\n");
      out.write("\t<!-- Slicknav -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/slicknav.min.css\">\n");
      out.write("\t\n");
      out.write("\t<!-- Eshop StyleSheet -->\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/reset.css\">\n");
      out.write("\t<link rel=\"stylesheet\" href=\"style.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/responsive.css\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\t\n");
      out.write("\t\t\n");
      out.write("\t\t<!-- Header -->\n");
      out.write("\t\t<header class=\"header shop\">\n");
      out.write("\t\t\t<div class=\"middle-inner\">\n");
      out.write("<!--\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t<div class=\"row\">-->\n");
      out.write("\t\t\t\t\t\t<!--<div class=\"col-lg-2 col-md-3 col-12\">-->\n");
      out.write("\t\t\t\t\t\t\t<div class=\"right-bar\">\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"sinlge-bar shopping\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"single-icon\"><i class=\"ti-bag\"></i> <span class=\"total-count\">2</span></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t<!-- Shopping Item -->\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"shopping-item\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown-cart-header\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span>2 Items</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">View Cart</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<ul class=\"shopping-list\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"remove\" title=\"Remove this item\"><i class=\"fa fa-remove\"></i></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"cart-img\" href=\"#\"><img src=\"https://via.placeholder.com/70x70\" alt=\"#\"></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<h4><a href=\"#\">Woman Ring</a></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"quantity\">1x - <span class=\"amount\">$99.00</span></p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"remove\" title=\"Remove this item\"><i class=\"fa fa-remove\"></i></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"cart-img\" href=\"#\"><img src=\"https://via.placeholder.com/70x70\" alt=\"#\"></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<h4><a href=\"#\">Woman Necklace</a></h4>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"quantity\">1x - <span class=\"amount\">$35.00</span></p>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"bottom\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<div class=\"total\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<span>Total</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"total-amount\">$134.00</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a href=\"checkout.html\" class=\"btn animate\">Checkout</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<!--</div>-->\n");
      out.write("<!--\t\t\t\t</div>\n");
      out.write("\t\t\t</div>-->\n");
      out.write("\t\t</header>\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write("\t\t\n");
      out.write("\t\n");
      out.write("\t<!-- Jquery -->\n");
      out.write("    <script src=\"js/jquery.min.js\"></script>\n");
      out.write("    <script src=\"js/jquery-migrate-3.0.0.js\"></script>\n");
      out.write("\t<script src=\"js/jquery-ui.min.js\"></script>\n");
      out.write("\t<!-- Popper JS -->\n");
      out.write("\t<script src=\"js/popper.min.js\"></script>\n");
      out.write("\t<!-- Bootstrap JS -->\n");
      out.write("\t<script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("\t<!-- Color JS -->\n");
      out.write("\t<script src=\"js/colors.js\"></script>\n");
      out.write("\t<!-- Slicknav JS -->\n");
      out.write("\t<script src=\"js/slicknav.min.js\"></script>\n");
      out.write("\t<!-- Owl Carousel JS -->\n");
      out.write("\t<script src=\"js/owl-carousel.js\"></script>\n");
      out.write("\t<!-- Magnific Popup JS -->\n");
      out.write("\t<script src=\"js/magnific-popup.js\"></script>\n");
      out.write("\t<!-- Fancybox JS -->\n");
      out.write("\t<script src=\"js/facnybox.min.js\"></script>\n");
      out.write("\t<!-- Waypoints JS -->\n");
      out.write("\t<script src=\"js/waypoints.min.js\"></script>\n");
      out.write("\t<!-- Countdown JS -->\n");
      out.write("\t<script src=\"js/finalcountdown.min.js\"></script>\n");
      out.write("\t<!-- Nice Select JS -->\n");
      out.write("\t<script src=\"js/nicesellect.js\"></script>\n");
      out.write("\t<!-- Ytplayer JS -->\n");
      out.write("\t<script src=\"js/ytplayer.min.js\"></script>\n");
      out.write("\t<!-- Flex Slider JS -->\n");
      out.write("\t<script src=\"js/flex-slider.js\"></script>\n");
      out.write("\t<!-- ScrollUp JS -->\n");
      out.write("\t<script src=\"js/scrollup.js\"></script>\n");
      out.write("\t<!-- Onepage Nav JS -->\n");
      out.write("\t<script src=\"js/onepage-nav.min.js\"></script>\n");
      out.write("\t<!-- Easing JS -->\n");
      out.write("\t<script src=\"js/easing.js\"></script>\n");
      out.write("\t<!-- Active JS -->\n");
      out.write("\t<script src=\"js/active.js\"></script>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
