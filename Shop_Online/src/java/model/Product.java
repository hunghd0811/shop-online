/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Black
 */
@Builder
@Getter
@Setter
@ToString
public class Product {
    private int id;
    private String name;
    private String description;
    private int quantity;
    private String image;
    private double price;
    private int categoryid;
    private int sellid;

    public Product() {
    }

    public Product(int id, String name, String description, int quantity, String image, double price, int categoryid, int sellid) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.image = image;
        this.price = price;
        this.categoryid = categoryid;
        this.sellid = sellid;
    }

     

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }


    public void setPrice(double price) {
        this.price = price;
    }

    public int getSellid() {
        return sellid;
    }

    public void setSellid(int sellid) {
        this.sellid = sellid;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", description=" + description + ", quantity=" + quantity + ", image=" + image + ", price=" + price + ", categoryid=" + categoryid + ", sellid=" + sellid + '}';
    }

    
    
}
