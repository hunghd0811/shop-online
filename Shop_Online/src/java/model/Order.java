/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Black
 */
@Builder
@Getter
@Setter
@ToString
public class Order {

    private int orderid;
    private int adminid;
    private double totalprice;
    private String note;
    private String created_date;
    private int customerid;
}
