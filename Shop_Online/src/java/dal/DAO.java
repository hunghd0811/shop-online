/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Account;
import model.Cart;
import model.Category;
import model.Customer;
import model.Order;
import model.Product;

/**
 *
 * @author Black
 */
public class DAO extends DBContext {

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList();
        String query = "select * from Product";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Product> sortPriceMintoMax() {
        List<Product> list = new ArrayList();
        String query = "select * from Product order by Price";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Product> getAllProductByCategoryID(String categoryid) {
        List<Product> list = new ArrayList();
        String query = "select * from Product where CategoryID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, categoryid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Product> getAllProductBySellID(int id) {
        List<Product> list = new ArrayList();
        String query = "select * from Product where SellID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Product getAllProductByID(String productid) {
        String query = "select * from Product where ProductID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, productid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Product getProductByID(int productid) {
        String query = "select * from Product where ProductID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, productid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Category> getAllCategory() {
        List<Category> list = new ArrayList();
        String query = "select * from Category";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1),
                        rs.getString(2)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Product> searchByName(String txtsearch) {
        List<Product> list = new ArrayList();
        String query = "select * from Product where ProductName like ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, "%" + txtsearch + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Account login(String username, String password) {
        String query = "select * from Account where Username = ? and [Password] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Account checkAccountExist(String username) {
        String query = "select * from Account where Username = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public void signup(String username, String password, String email) {
        String query = "insert into Account values(?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, username);
            st.setString(2, password);
            st.setString(3, email);

            st.executeUpdate();

        } catch (SQLException e) {
        }
    }

    public void deleteProduct(String productid) {
        String query = "delete from Product where ProductID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, productid);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public void insertProduct(String productname, String description, String quantity, String image, String price, String category, int sellid) {
        String query = "INSERT [dbo].[Product] ([ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (?,?,?,?,?,?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, productname);
            st.setString(2, description);
            st.setString(3, quantity);
            st.setString(4, image);
            st.setString(5, price);
            st.setString(6, category);
            st.setInt(7, sellid);

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public void updateProduct(String name, String description, String quantity, String image, String price, String categoryid, String id) {
        String query = "UPDATE [dbo].[Product]\n"
                + "   SET [ProductName] = ? \n"
                + "      ,[Description] = ?\n"
                + "      ,[Quantity] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[Price] = ?\n"
                + "      ,[CategoryID] = ?\n"
                + "      \n"
                + " WHERE ProductID = ?";

        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, name);
            st.setString(2, description);
            st.setString(3, quantity);
            st.setString(4, image);
            st.setString(5, price);
            st.setString(6, categoryid);
            st.setString(7, id);

            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public static void main(String[] args) {
        // Product p = new Product(1, "dsf", "sfdsf", 2, "dsfds", 4, 5, 1);
        DAO d = new DAO();
        d.updateProduct("fsdf", "sdfsd", "10", "aasd", "5", "5", "2");
    }

    public int createReturnID(Customer customer) {
        String query = "INSERT INTO [dbo].[Customer]\n"
                + "           ([Fullname]\n"
                + "           ,[Phone]\n"
                + "           ,[Email]\n"
                + "           ,[Address])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, customer.getName());
            st.setString(2, customer.getPhone());
            st.setString(3, customer.getEmail());
            st.setString(4, customer.getAddress());
            st.executeUpdate();

            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public int createReturnID(Order order) {
        String query = "INSERT INTO [dbo].[Orders]\n"
                + "           ([AdminID]\n"
                + "           ,[TotalPrice]\n"
                + "           ,[Note]\n"
                + "           ,[CustomerID])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, order.getAdminid());
            st.setDouble(2, order.getTotalprice());
            st.setString(3, order.getNote());
            st.setInt(4, order.getCustomerid());
            st.executeUpdate();

            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;

    }

    public void saveCart(int orderid, Map<Integer, Cart> carts) {
        String query = "INSERT INTO [dbo].[OrderDetails]\n"
                + "           ([OrdersID]\n"
                + "           ,[Name]\n"
                + "           ,[Image]\n"
                + "           ,[Price]\n"
                + "           ,[Quantity])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, orderid);
            for (Map.Entry<Integer, Cart> entry : carts.entrySet()) {
                Integer productid = entry.getKey();
                Cart cart = entry.getValue();
                st.setString(2, cart.getProduct().getName());
                st.setString(3, cart.getProduct().getImage());
                st.setDouble(4, cart.getProduct().getPrice());
                st.setInt(5, cart.getQuantity());
                st.executeUpdate();
            }

        } catch (SQLException e) {
        }
    }

}
