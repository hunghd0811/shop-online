USE [Project]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/22/2022 2:26:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](255) NULL,
	[Password] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[isSell] [int] NULL,
	[isAdmin] [int] NULL,
	[Role] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/22/2022 2:26:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 3/22/2022 2:26:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[Fullname] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 3/22/2022 2:26:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[OrderDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[OrdersID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[Image] [nvarchar](max) NULL,
	[Price] [float] NULL,
	[Quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 3/22/2022 2:26:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrdersID] [int] IDENTITY(1,1) NOT NULL,
	[AdminID] [int] NULL,
	[TotalPrice] [float] NULL,
	[Note] [nvarchar](max) NULL,
	[Created_date] [date] NULL,
	[CustomerID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrdersID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 3/22/2022 2:26:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](1000) NULL,
	[Description] [nvarchar](max) NULL,
	[Quantity] [int] NULL,
	[image] [nvarchar](max) NULL,
	[Price] [money] NULL,
	[CategoryID] [int] NULL,
	[SellID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([AdminID], [Username], [Password], [Email], [isSell], [isAdmin], [Role]) VALUES (1, N'anh', N'123', N'anh@gmail.com', 1, 1, N'ADMIN')
INSERT [dbo].[Account] ([AdminID], [Username], [Password], [Email], [isSell], [isAdmin], [Role]) VALUES (2, N'em', N'123', N'em@gmail.com', 1, 1, N'ADMIN')
INSERT [dbo].[Account] ([AdminID], [Username], [Password], [Email], [isSell], [isAdmin], [Role]) VALUES (3, N'anan', N'123456', N'anan@gmail.com', 1, 0, N'USER')
INSERT [dbo].[Account] ([AdminID], [Username], [Password], [Email], [isSell], [isAdmin], [Role]) VALUES (4, N'huy', N'huy123', N'huy@gmail.com', 1, 0, N'USER')
INSERT [dbo].[Account] ([AdminID], [Username], [Password], [Email], [isSell], [isAdmin], [Role]) VALUES (5, N'duong', N'duong', N'duong@gmail.com', 1, 1, N'ADMIN')
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (1, N'IPHONE 13')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (2, N'IPHONE 12')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (3, N'IPHONE 11')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (4, N'IPHONE X')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (5, N'IPHONE SE')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (6, N'IPHONE 8')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (7, N'IPHONE 7')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (8, N'IPHONE 6')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (9, N'IPAD')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (10, N'MAC')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (11, N'APPLE WATCH')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (12, N'AIRPODS')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (1, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (2, N'add', N'0987654321', N'anhh@gmail.com', N'Ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (3, N'add', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (4, N'add', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (5, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (6, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (7, N'add', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (8, N'add', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (9, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'Ha noi1111')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (10, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'Ha noi222222')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (11, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (12, N'Nguyen Van A', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (13, N'add', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (14, N'add', N'0987654321', N'anhh@gmail.com', N'ha noi')
INSERT [dbo].[Customer] ([CustomerID], [Fullname], [Phone], [Email], [Address]) VALUES (15, N'hunghdhe153474', N'0987654321', N'hunghd0811@gmail.com', N'Ha noi222222')
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderDetails] ON 

INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (1, 1, N'IPHONE 13 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-13-1.jpg', 27, 2)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (2, 1, N'IPHONE 12', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-12-2(1).JPG', 25, 2)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (3, 1, N'IPHONE 12 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-12-pro-2.jpg', 14, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (4, 2, N'IPHONE 13 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-13-1.jpg', 27, 3)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (5, 2, N'IPHONE 12', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-12-2(1).JPG', 25, 2)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (6, 2, N'IPHONE 12 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-12-pro-2.jpg', 14, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (7, 2, N'IPHONE 12 PROMAX', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-12-pro-max-1.JPG', 25, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (8, 2, N'SAMSUNG GALAXY S22 ULTRA 5G', N'https://fptshop.com.vn/landing-galaxy-s22/Content/images/anh-tt4.jpg?v=202203032132', 35, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (9, 3, N'IPHONE 12 PROMAX', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-12-pro-max-1.JPG', 25, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (10, 5, N'IPHONE 13 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-13-1.jpg', 27, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (11, 6, N'IPHONE 13 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-13-1.jpg', 27, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (12, 7, N'IPHONE 13 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-13-1.jpg', 27, 2)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (13, 8, N'IPHONE 13 PRO', N'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-13-1.jpg', 27, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (14, 9, N'IPHONE 13 PRO', N'./images/ip/iphone13pro.png', 999, 3)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (15, 10, N'IPHONE 12 PRO', N'./images/ip/iphone12pro.png', 899, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (16, 11, N'IPHONE 12', N'./images/ip/iphone12.png', 749, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (17, 11, N'IPHONE 12 PRO', N'./images/ip/iphone12pro.png', 899, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (18, 12, N'IPHONE 12', N'./images/ip/iphone12.png', 749, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (19, 12, N'IPHONE 12 PRO', N'./images/ip/iphone12pro.png', 899, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (20, 13, N'IPHONE 12', N'./images/ip/iphone12.png', 749, 2)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (21, 14, N'IPHONE 12', N'./images/ip/iphone12.png', 749, 1)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (22, 15, N'IPHONE 12 PRO', N'./images/ip/iphone12pro.png', 899, 2)
INSERT [dbo].[OrderDetails] ([OrderDetailsID], [OrdersID], [Name], [Image], [Price], [Quantity]) VALUES (23, 15, N'IPHONE 12', N'./images/ip/iphone12.png', 749, 1)
SET IDENTITY_INSERT [dbo].[OrderDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (1, 1, 118, N'nope', NULL, 1)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (2, 1, 205, N'nope nope', CAST(N'2022-03-16' AS Date), 2)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (3, 1, 2.5, N'nope', CAST(N'2022-03-16' AS Date), 3)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (4, 1, 0, N'nope', CAST(N'2022-03-16' AS Date), 4)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (5, 1, 2.7, N'nope', CAST(N'2022-03-16' AS Date), 5)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (6, 1, 29.7, N'nope nope', CAST(N'2022-03-16' AS Date), 6)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (7, 1, 59.4, N'nope', CAST(N'2022-03-16' AS Date), 7)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (8, 1, 29.7, N'nope', CAST(N'2022-03-16' AS Date), 8)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (9, 1, 3296.7, N'nope nope', CAST(N'2022-03-17' AS Date), 9)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (10, 1, 988.9, N'nope nope', CAST(N'2022-03-17' AS Date), 10)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (11, 1, 1812.8, N'nope nope', CAST(N'2022-03-22' AS Date), 11)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (12, 4, 1812.8, N'nope', CAST(N'2022-03-22' AS Date), 12)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (13, 1, 1647.8, N'nope', CAST(N'2022-03-22' AS Date), 13)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (14, 1, 823.9, N'nope', CAST(N'2022-03-22' AS Date), 14)
INSERT [dbo].[Orders] ([OrdersID], [AdminID], [TotalPrice], [Note], [Created_date], [CustomerID]) VALUES (15, 1, 2801.7, N'nope nope', CAST(N'2022-03-22' AS Date), 15)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (3, N'IPHONE 13 PRO', N'Alpine green, sierra blue, silver, gold, and graphite
6.1-inch Super Retina XDR display with ProMotion technology, HDR, and True Tone1
Ceramic Shield front, textured matte glass back and stainless steel design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Pro 12MP camera system (Telephoto, Wide, and Ultra Wide) with Portrait mode, Night mode, Night mode portraits, Deep Fusion, Smart HDR 4, Photographic Styles, Apple ProRAW, ProRes, Cinematic mode in 1080p at 30 fps, and Dolby Vision HDR video recording up to 4K at 60 fps
LiDAR Scanner for Night mode portraits, faster autofocus in low light, and improved AR experiences
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 4, Photographic Styles, Cinematic mode in 1080p at 30 fps, Dolby Vision HDR video recording up to 4K at 60 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A15 Bionic chip with new 6-core CPU with 2 performance and 4 efficiency cores, 5-core GPU, and 16-core Neural Engine
Battery life: Up to 22 hours video playback; up to 20 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone13pro.png', 999.0000, 1, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (4, N'IPHONE 12', N'Black, white, (PRODUCT)RED,
green, blue, and purple
6.1-inch Super Retina XDR display with HDR and True Tone1
Ceramic Shield front, glass back and aluminum design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Dual 12MP camera system (Wide and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, Smart HDR 3, Dolby Vision HDR video recording up to 4K at 30 fps, and 4K video up to 60 fps with extended dynamic range
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 3, Dolby Vision HDR video recording up to 4K at 30 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A14 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 17 hours video playback; up to 11 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone12.png', 749.0000, 2, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (5, N'IPHONE 12 PRO', N'Graphite, silver, gold, and pacific blue
6.1-inch Super Retina XDR display with HDR and True Tone1
Ceramic Shield front, textured matte glass back and stainless steel design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Pro 12MP camera system (Telephoto, Wide, and Ultra Wide) with Portrait mode, Night mode, Night mode portraits, Deep Fusion, Smart HDR 3, Apple ProRAW, and Dolby Vision HDR video recording up to 4K at 60 fps
LiDAR Scanner for Night mode portraits, faster autofocus in low light, and improved AR experiences
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 3, Dolby Vision HDR video recording up to 4K at 30 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A14 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 17 hours video playback; up to 11 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone12pro.png', 899.0000, 2, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (6, N'IPHONE 12 PRO MAX', N'Graphite, silver, gold, and pacific blue
6.7-inch Super Retina XDR display with HDR and True Tone1
Ceramic Shield front, textured matte glass back and stainless steel design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Pro 12MP camera system (Telephoto, Wide, and Ultra Wide) with Portrait mode, Night mode, Night mode portraits, Deep Fusion, Smart HDR 3, Apple ProRAW, and Dolby Vision HDR video recording up to 4K at 60 fps
LiDAR Scanner for Night mode portraits, faster autofocus in low light, and improved AR experiences
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 3, Dolby Vision HDR video recording up to 4K at 30 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A14 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 20 hours video playback; up to 12 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone12promax.png', 1149.0000, 2, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (7, N'IPHONE 11', N'Black, white, purple, green, yellow, and (PRODUCT)RED
6.1-inch Liquid Retina HD display with True Tone1
Glass and aluminum design
Water resistant to a depth of 2 meters for up to 30 minutes (IP68)4
Dual 12MP cameras (Wide and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, next-generation Smart HDR, and 4K video up to 60 fps with extended dynamic range
12MP TrueDepth front camera with Portrait mode, next-generation Smart HDR, 4K video recording up to 60 fps, and slo-mo video support for 1080p at 120 fps
Gigabit-class LTE2
A13 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 17 hours video playback; up to 10 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone11.png', 539.0000, 3, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (8, N'IPHONE 11 PRO', N'Silver, space gray, gold, and midnight green
5.8-inch Super Retina XDR display with HDR and True Tone1
Textured matte glass and stainless steel design
Water resistant to a depth of 4 meters for up to 30 minutes (IP68)4
Triple 12MP cameras (Telephoto, Wide, and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, next-generation Smart HDR, and 4K video up to 60 fps with extended dynamic range
12MP TrueDepth front camera with Portrait mode, next-generation Smart HDR, 4K video recording up to 60 fps, and slo-mo video support for 1080p at 120 fps
Gigabit LTE2
A13 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 18 hours video playback; up to 11 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast charge with 18W adapter included', 50, N'./images/ip/iphone11pro.png', 579.0000, 3, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (9, N'IPHONE 11 PRO MAX', N'Silver, space gray, gold, and midnight green
6.5-inch Super Retina XDR display with HDR and True Tone1
Textured matte glass and stainless steel design
Water resistant to a depth of 4 meters for up to 30 minutes (IP68)4
Triple 12MP cameras (Telephoto, Wide, and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, next-generation Smart HDR, and 4K video up to 60 fps with extended dynamic range
12MP TrueDepth front camera with Portrait mode, next-generation Smart HDR, 4K video recording up to 60 fps, and slo-mo video support for 1080p at 120 fps
Gigabit LTE2
A13 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 20 hours video playback; up to 12 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast charge with 18W adapter included', 50, N'./images/ip/iphone11promax.png', 699.0000, 3, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (10, N'IPHONE 12 MINI', N'Black, white, (PRODUCT)RED,
green, blue, and purple
5.4-inch Super Retina XDR display with HDR and True Tone1
Ceramic Shield front, glass back and aluminum design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Dual 12MP camera system (Wide and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, Smart HDR 3, Dolby Vision HDR video recording up to 4K at 30 fps, and 4K video up to 60 fps with extended dynamic range
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 3, Dolby Vision HDR video recording up to 4K at 30 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A14 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 15 hours video playback; up to 10 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone12mini.png', 699.0000, 2, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (11, N'IPHONE 13 MINI', N'Green, pink, blue, midnight, starlight, and (PRODUCT)RED
5.4-inch Super Retina XDR display with HDR and True Tone1
Ceramic Shield front, glass back and aluminum design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Dual 12MP camera system (Wide and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, Smart HDR 4, Photographic Styles, Cinematic mode in 1080p at 30 fps, and Dolby Vision HDR video recording up to 4K at 60 fps
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 4, Photographic Styles, Cinematic mode in 1080p at 30 fps, Dolby Vision HDR video recording up to 4K at 60 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A15 Bionic chip with new 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 17 hours video playback; up to 13 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone13mini.png', 699.0000, 1, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (12, N'IPHONE 13', N'Green, pink, blue, midnight, starlight, and (PRODUCT)RED
6.1-inch Super Retina XDR display with HDR and True Tone1
Ceramic Shield front, glass back and aluminum design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Dual 12MP camera system (Wide and Ultra Wide) with Portrait mode, Night mode, Deep Fusion, Smart HDR 4, Photographic Styles, Cinematic mode in 1080p at 30 fps, and Dolby Vision HDR video recording up to 4K at 60 fps
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 4, Photographic Styles, Cinematic mode in 1080p at 30 fps, Dolby Vision HDR video recording up to 4K at 60 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A15 Bionic chip with new 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 19 hours video playback; up to 15 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone13.png', 799.0000, 1, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (13, N'IPHONE 13 PRO MAX', N'Alpine green, sierra blue, silver, gold, and graphite
6.7-inch Super Retina XDR display with ProMotion technology, HDR, and True Tone1
Ceramic Shield front, textured matte glass back and stainless steel design
Water resistant to a depth of 6 meters for up to 30 minutes (IP68)4
Pro 12MP camera system (Telephoto, Wide, and Ultra Wide) with Portrait mode, Night mode, Night mode portraits, Deep Fusion, Smart HDR 4, Photographic Styles, Apple ProRAW, ProRes, Cinematic mode in 1080p at 30 fps, and Dolby Vision HDR video recording up to 4K at 60 fps
LiDAR Scanner for Night mode portraits, faster autofocus in low light, and improved AR experiences
12MP TrueDepth front camera with Portrait mode, Night mode, Deep Fusion, Smart HDR 4, Photographic Styles, Cinematic mode in 1080p at 30 fps, Dolby Vision HDR video recording up to 4K at 60 fps, and slo-mo video support for 1080p at 120 fps
5G and Gigabit LTE2
A15 Bionic chip with new 6-core CPU with 2 performance and 4 efficiency cores, 5-core GPU, and 16-core Neural Engine
Battery life: Up to 28 hours video playback; up to 25 hours video playback (streamed)3
Face ID for secure authentication and Apple Pay
Compatible with MagSafe accessories
MagSafe and Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone13promax.png', 1099.0000, 1, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (14, N'IPHONE SE (2nd generation)', N'Black, white, and (PRODUCT)RED
4.7-inch Retina HD display with True Tone
Glass and aluminum design
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Single 12MP camera (Wide) with Portrait mode, next-generation Smart HDR, and 4K video up to 60 fps with extended dynamic range up to 30 fps
7MP FaceTime HD camera with Portrait mode, Auto HDR, and 1080p HD video recording at 30 fps
Gigabit-class LTE2
A13 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 13 hours video playback; up to 8 hours video playback (streamed)3
Touch ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphonese2.png', 579.0000, 5, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (15, N'IPHONE XS MAX', N'Silver, space gray, and gold
6.5-inch Super Retina HD display with HDR and True Tone1
Glass and stainless steel design
Water resistant to a depth of 2 meters for up to 30 minutes (IP68)4
Dual 12MP cameras (Telephoto and Wide) with Portrait mode, Smart HDR, and 4K video up to 60 fps with extended dynamic range up to 30 fps
7MP TrueDepth front camera with Portrait mode, Smart HDR, and 1080p HD video recording up to 60 fps
Gigabit-class LTE2
A12 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 15 hours video playback3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphonexsmax.png', 449.0000, 4, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (16, N'IPHONE XS', N'Silver, space gray, and gold
5.8-inch Super Retina HD display with HDR and True Tone1
Glass and stainless steel design
Water resistant to a depth of 2 meters for up to 30 minutes (IP68)4
Dual 12MP cameras (Telephoto and Wide) with Portrait mode, Smart HDR, and 4K video up to 60 fps with extended dynamic range up to 30 fps
7MP TrueDepth front camera with Portrait mode, Smart HDR, and 1080p HD video recording up to 60 fps
Gigabit-class LTE2
A12 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 14 hours video playback3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphonexs.png', 399.0000, 4, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (17, N'IPHONE XR', N'Black, white, blue, coral, yellow, and (PRODUCT)RED
6.1-inch Liquid Retina HD display with True Tone1
Glass and aluminum design
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Single 12MP camera (Wide) with Portrait mode, Smart HDR, and 4K video up to 60 fps with extended dynamic range up to 30 fps
7MP TrueDepth front camera with Portrait mode, Smart HDR, and 1080p HD video recording up to 60 fps
LTE Advanced2
A12 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 8-core Neural Engine
Battery life: Up to 16 hours video playback3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphonexr.png', 550.0000, 4, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (18, N'IPHONE X', N'Silver and space gray
5.8-inch Super Retina HD display with HDR and True Tone1
Glass and stainless steel design
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Dual 12MP cameras (Telephoto and Wide) with Portrait mode, Auto HDR, and 4K video up to 60 fps
7MP TrueDepth front camera with Portrait mode, Auto HDR, and 1080p HD video recording at 30 fps
LTE Advanced2
A11 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 3-core GPU, and 2-core Neural Engine
Battery life: Up to 13 hours video playback3
Face ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphonex.png', 279.0000, 4, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (19, N'IPHONE 8 PLUS', N'Silver, space gray, and gold
5.5-inch Retina HD display with True Tone
Glass and aluminum design
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Dual 12MP cameras (Telephoto and Wide) with Portrait mode, Auto HDR, and 4K video up to 60 fps
7MP FaceTime HD camera with Auto HDR and 1080p HD video recording at 30 fps
LTE Advanced2
A11 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 3-core GPU, and 2-core Neural Engine
Battery life: Up to 14 hours video playback3
Touch ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone8plus.png', 339.0000, 6, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (20, N'IPHONE 8', N'Silver, space gray, and gold
4.7-inch Retina HD display with True Tone
Glass and aluminum design
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Single 12MP camera (Wide) with Auto HDR and 4K video up to 60 fps
7MP FaceTime HD camera with Auto HDR and 1080p HD video recording at 30 fps
LTE Advanced2
A11 Bionic chip with 6-core CPU with 2 performance and 4 efficiency cores, 3-core GPU, and 2-core Neural Engine
Battery life: Up to 13 hours video playback3
Touch ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphone8.png', 249.0000, 6, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (21, N'IPHONE 7 PLUS', N'Black, silver, gold, and rose gold
5.5-inch Retina HD display
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Dual 12MP cameras (Telephoto and Wide) with Portrait mode, HDR, and 4K video at 30 fps
7MP FaceTime HD camera with HDR and 1080p HD video recording at 30 fps
LTE Advanced2
A10 Fusion chip
Battery life: Up to 14 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphone7plus.png', 150.0000, 7, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (22, N'IPHONE 7', N'Black, silver, gold, and rose gold
4.7-inch Retina HD display
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Single 12MP camera (Wide) with HDR and 4K video at 30 fps
7MP FaceTime HD camera with HDR and 1080p HD video recording at 30 fps
LTE Advanced2
A10 Fusion chip
Battery life: Up to 13 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphone7.png', 92.0000, 7, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (23, N'IPHONE 6s PLUS', N'Silver, space gray, gold, and rose gold
5.5-inch Retina HD display
Single 12MP camera (Wide) with HDR and 4K video at 30 fps
5MP FaceTime HD camera with HDR and 720p HD video recording at 30 fps
LTE Advanced2
A9 chip
Battery life: Up to 14 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphone6splus.png', 95.0000, 8, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (24, N'IPHONE 6s', N'Silver, space gray, gold, and rose gold
4.7-inch Retina HD display
Single 12MP camera (Wide) with HDR and 4K video at 30 fps
5MP FaceTime HD camera with HDR and 720p HD video recording at 30 fps
LTE Advanced2
A9 chip
Battery life: Up to 11 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphone6s.png', 55.0000, 8, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (25, N'IPHONE 6 PLUS', N'Silver, space gray, and gold
5.5-inch Retina HD display
Single 8MP camera (Wide) with HDR and 1080p HD video up to 60 fps
1.2MP FaceTime HD camera with HDR and 720p HD video recording at 30 fps
LTE2
A8 chip
Battery life: Up to 14 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphone6plus.png', 75.0000, 8, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (26, N'IPHONE 6 ', N'Silver, space gray, and gold
4.7-inch Retina HD display
Single 8MP camera (Wide) with HDR and 1080p HD video up to 60 fps
1.2MP FaceTime HD camera with HDR and 720p HD video recording at 30 fps
LTE2
A8 chip
Battery life: Up to 11 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphone6.png', 40.0000, 8, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (27, N'IPHONE SE (1nd generation)', N'Silver, space gray, gold, and rose gold
4-inch Retina display
Single 12MP camera (Wide) with HDR and 4K video at 30 fps
1.2MP FaceTime HD camera with HDR and 720p HD video recording at 30 fps
LTE2
A9 chip
Battery life: Up to 13 hours video playback3
Touch ID for secure authentication and Apple Pay', 50, N'./images/ip/iphonese1.png', 299.0000, 5, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (28, N'IPAD PRO 12.9-in (5th generation)', N'Silver and space gray
12.9‑inch Liquid Retina XDR display with ProMotion technology and True Tone1
Face ID for secure authentication and Apple Pay
Apple M1 chip with next-generation Neural Engine
12MP Wide and 10MP Ultra Wide cameras with Smart HDR 3 and 4K video at 24 fps, 25 fps, 30 fps, or 60 fps
TrueDepth camera with 12MP Ultra Wide front camera, Center Stage, Portrait mode, Portrait Lighting, and Smart HDR 3
LiDAR Scanner
5G and Gigabit LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB-C connector with support for Thunderbolt / USB 4', 50, N'./images/ip/ipadpro12.95.png', 1299.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (33, N'IPAD PRO 11-in (3rd generation)', N'Silver and space gray
11‑inch Liquid Retina display with ProMotion technology and True Tone1
Face ID for secure authentication and Apple Pay
Apple M1 chip with next-generation Neural Engine
12MP Wide and 10MP Ultra Wide cameras with Smart HDR 3 and 4K video at 24 fps, 25 fps, 30 fps, or 60 fps
TrueDepth camera with 12MP Ultra Wide front camera, Center Stage, Portrait mode, Portrait Lighting, and Smart HDR 3
LiDAR Scanner
5G and Gigabit LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB-C connector with support for Thunderbolt / USB 4', 50, N'./images/ip/ipadpro113.png', 999.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (43, N'IPAD AIR (5th  generation)', N'Space gray, starlight, pink, purple, and blue
10.9‑inch Liquid Retina display with True Tone1
Touch ID for secure authentication and Apple Pay
Apple M1 chip with next-generation Neural Engine
12MP Wide camera with Smart HDR 3 and 4K video at 24 fps, 25 fps, 30 fps, or 60 fps
12MP Ultra Wide front camera with Center Stage and Smart HDR 3
5G and Gigabit LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB‑C connector', 50, N'./images/ip/ipadair5.png', 749.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (44, N'IPAD (9th genartion)', N'Silver and space gray
10.2‑inch Retina display with True Tone
Touch ID for secure authentication and Apple Pay
A13 Bionic chip with Neural Engine
8MP Wide camera with HDR and 1080p HD video
12MP Ultra Wide front camera with Center Stage and HDR
Gigabit-class LTE2
Works with Apple Pencil (1st generation)
Works with Smart Keyboard and Bluetooth keyboards
Lightning connector', 50, N'./images/ip/ipad9.png', 459.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (45, N'IPAD MINI (6th generation)', N'Purple, pink, starlight, and space gray
8.3-inch Liquid Retina display with True Tone1
Touch ID for secure authentication and Apple Pay
A15 Bionic chip with Neural Engine
12MP Wide camera with Smart HDR 3 and 4K video at 24 fps, 25 fps, 30 fps, or 60 fps
12MP Ultra Wide front camera with Center Stage and Smart HDR 3
5G and Gigabit LTE2
Works with Apple Pencil (2nd generation)
Works with Bluetooth keyboards
USB-C connector', 50, N'./images/ip/ipadmini6.png', 649.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (46, N'IPAD PRO 12.9-in (4th generation)', N'Silver and space gray
12.9‑inch Liquid Retina display with ProMotion technology and True Tone1
Face ID for secure authentication and Apple Pay
A12Z Bionic chip with Neural Engine
12MP Wide and 10MP Ultra Wide cameras with Smart HDR and 4K video at 24 fps, 25 fps, 30 fps, or 60 fps
7MP TrueDepth front camera with Portrait mode, Portrait Lighting, and Smart HDR
LiDAR Scanner
Gigabit-class LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB‑C connector', 50, N'./images/ip/ipadpro12.94.png', 999.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (47, N'IPAD PRO 12.9-in (3rd generation)', N'Silver and space gray
12.9‑inch Liquid Retina display with ProMotion technology and True Tone1
Face ID for secure authentication and Apple Pay
A12X Bionic chip with Neural Engine
12MP Wide camera with Smart HDR and 4K video at 30 fps or 60 fps
7MP TrueDepth front camera with Portrait mode, Portrait Lighting, and Smart HDR
Gigabit-class LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB‑C connector', 50, N'./images/ip/ipadpro12.93.png', 899.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (48, N'IPAD PRO 12.9-in (2nd generation)', N'Silver, space gray, and gold
12.9‑inch Retina display with ProMotion technology and True Tone
Touch ID for secure authentication and Apple Pay
A10X Fusion chip
12MP Wide camera with Auto HDR and 4K video at 30 fps
7MP FaceTime HD front camera with Auto HDR
LTE Advanced2
Works with Apple Pencil (1st generation)
Works with Smart Keyboard and Bluetooth keyboards
Lightning connector', 50, N'./images/ip/ipadpro12.92.png', 799.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (49, N'IPAD PRO 12.9-in (1st generation)', N'Silver, space gray, and gold
12.9‑inch Retina display
Touch ID for secure authentication and Apple Pay
A9X chip
8MP Wide camera with HDR and 1080p video
1.2MP FaceTime HD front camera with Auto HDR
LTE2
Works with Apple Pencil (1st generation)
Works with Smart Keyboard and Bluetooth keyboards
Lightning connector', 50, N'./images/ip/ipadpro12.91.png', 699.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (51, N'IPAD PRO 11-in (2nd generation)', N'Silver and space gray
11‑inch Liquid Retina display with ProMotion technology and True Tone1
Face ID for secure authentication and Apple Pay
A12Z Bionic chip with Neural Engine
12MP Wide and 10MP Ultra Wide cameras with Smart HDR and 4K video at 24 fps, 25 fps, 30 fps, or 60 fps
7MP TrueDepth front camera with Portrait mode, Portrait Lighting, and Smart HDR
LiDAR Scanner
Gigabit-class LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB‑C connector', 50, N'./images/ip/ipadpro112.png', 899.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (52, N'IPAD PRO 11-in (1st generation)', N'Silver and space gray
11‑inch Liquid Retina display with ProMotion technology and True Tone1
Face ID for secure authentication and Apple Pay
A12X Bionic chip with Neural Engine
12MP Wide camera with Smart HDR and 4K video at 30 fps or 60 fps
7MP TrueDepth front camera with Portrait mode, Portrait Lighting, and Smart HDR
Gigabit-class LTE2
Works with Apple Pencil (2nd generation)
Works with Magic Keyboard, Smart Keyboard Folio, and Bluetooth keyboards
USB‑C connector', 50, N'./images/ip/ipadpro111.png', 799.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (53, N'IPAD PRO 10.5-in', N'Silver, space gray, gold, and rose gold
10.5‑inch Retina display with ProMotion technology and True Tone
Touch ID for secure authentication and Apple Pay
A10X Fusion chip
12MP Wide camera with Auto HDR and 4K video at 30 fps
7MP FaceTime HD front camera with Auto HDR
LTE Advanced2
Works with Apple Pencil (1st generation)
Works with Smart Keyboard and Bluetooth keyboards
Lightning connector', 50, N'./images/ip/ipadpro10.5.png', 549.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (54, N'IPAD PRO 9.7-in', N'Silver, space gray, gold, and rose gold
9.7‑inch Retina display with True Tone
Touch ID for secure authentication and Apple Pay
A9X chip
12MP Wide camera with Auto HDR and 4K video at 30 fps
5MP FaceTime HD front camera with Auto HDR
LTE Advanced2
Works with Apple Pencil (1st generation)
Works with Smart Keyboard and Bluetooth keyboards
Lightning connector', 50, N'./images/ip/ipadpro9.7.png', 499.0000, 9, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (55, N'MACBOOK PRO 14-in (2021)', N'Touch ID
14.2-inch (diagonal) Liquid Retina XDR display8
Apple M1 Pro or Apple M1 Max chip
Up to 8TB storage3
3.5 pounds4', 50, N'./images/mac/macbookpro14in2021.png', 1999.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (56, N'MACBOOK PRO 16-in (2021)', N'Touch ID
16.2-inch (diagonal) Liquid Retina XDR display8
Apple M1 Pro or Apple M1 Max chip
Up to 8TB storage3
4.7 pounds4', 50, N'./images/mac/macbookpro16in2021.png', 2499.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (57, N'MACBOOK ARI (M1, 2020)', N'Touch ID
13.3-inch (diagonal) LED-backlit Retina display1
Apple M1 chip
Up to 2TB storage3
2.8 pounds4', 50, N'./images/mac/macbookairm12020.png', 999.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (58, N'MACBOOK ARI (RETINA, 2020)', N'Touch ID
13.3-inch (diagonal) LED-backlit Retina display1
Up to 1.2GHz quad-core Intel Core i7 processor
Up to 2TB storage3
2.8 pounds4', 50, N'./images/mac/macbookairretina2020.png', 1199.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (59, N'MACBOOK ARI (2017)', N'13.3-inch (diagonal) LED-backlit widescreen display1
Up to 2.2GHz dual-core Intel Core i7 processor
Up to 512GB storage3
2.96 pounds4', 50, N'./images/mac/macbookair2017.png', 999.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (60, N'MACBOOK PRO 13-in (M1, 2020)', N'Touch Bar and Touch ID
13.3-inch (diagonal) LED-backlit Retina display1
Apple M1 chip
Up to 2TB storage3
3.0 pounds4', 50, N'./images/mac/macbookpro13in2020.png', 1299.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (61, N'MACBOOK PRO 16-in (2019)', N'Touch Bar and Touch ID
16-inch (diagonal) LED-backlit Retina display1
Up to 2.4GHz 8-core Intel Core i9 processor
Up to 8TB storage3
4.3 pounds4', 50, N'./images/mac/macbookpro16in2021.png', 1399.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (62, N'IMAC 21.5-in', N'21.5-inch (diagonal) LED-backlit display1
2.3GHz dual-core Intel Core i5 processor
Up to 1TB storage3', 50, N'./images/mac/imac21.5.png', 999.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (63, N'IMAC 21.5-in (4K RETINA)', N'21.5-inch (diagonal) LED-backlit 4K Retina display1
Up to 3.2GHz 6-core Intel Core i7 processor
Up to 1TB storage3', 50, N'./images/mac/imac21.54kretina.png', 1199.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (64, N'IMAC 24-in', N'24-inch (diagonal) LED-backlit 4.5K Retina display2
Apple M1 chip
Up to 2TB storage3
Touch ID', 50, N'./images/mac/imac24in.png', 1499.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (65, N'IMAC 27-in', N'27-inch (diagonal) LED-backlit 5K Retina display1
Up to 3.6GHz 10-core Intel Core i9 processor
Up to 8TB storage3', 50, N'./images/mac/imac27in.png', 1699.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (66, N'MAC MINI (M1, 2020)', N'Apple M1 chip
Up to 2TB storage3', 50, N'./images/mac/macminim12020.png', 699.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (67, N'MAC MINI (2018)', N'Up to 3.2GHz 6-core Intel Core i7 processor
Up to 2TB storage3', 50, N'./images/mac/macmini2018.png', 1099.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (68, N'MAC MINI (2014)', N'Up to 3.0GHz dual-core Intel Core i7 processor
Up to 2TB storage3', 50, N'./images/mac/macmini2018.png', 899.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (69, N'MAC STUDIO (2022)', N'Apple M1 Max or Apple M1 Ultra chip
Up to 8TB storage3', 50, N'./images/mac/macstudio2022.png', 1999.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (70, N'MAC PRO', N'Up to 2.5GHz 28-core Intel Xeon W processor
Up to 8TB storage3', 50, N'./images/mac/macpro.png', 5999.0000, 10, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (71, N'APPLE WATCH SERIES 1', N'38mm or 42mm case
Retina OLED display
Ion-X glass display
S1P with dual-core processor
Digital Crown
Optical heart sensor
High and low heart rate notifications and irregular heart rhythm notification3
Splash resistant21
Wi-Fi and Bluetooth 4.0
Built-in speaker and mic
8GB capacity', 50, N'./images/mac/watch1.png', 99.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (72, N'APPLE WATCH SERIES 2', N'38mm or 42mm case
Second-generation Retina OLED display
Ion-X glass display on aluminum cases; sapphire crystal display on stainless steel and ceramic cases
GPS model
S2 with dual-core processor
Digital Crown
Optical heart sensor
High and low heart rate notifications and irregular heart rhythm notification3
Water resistant 50 meters1
Wi-Fi and Bluetooth 4.0
GPS/GNSS
Built-in speaker and mic
8GB capacity', 50, N'./images/mac/watch2.png', 149.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (73, N'APPLE WATCH SERIES 3', N'38mm or 42mm case
Second-generation Retina OLED display, 1000 nits
Ion-X glass display on aluminum cases; sapphire crystal display on stainless steel and ceramic cases
GPS model
S3 SiP with dual-core processor; W2 wireless chip
Digital Crown
Optical heart sensor
High and low heart rate notifications and irregular heart rhythm notification3
Emergency SOS4
Water resistant 50 meters1
Wi-Fi and Bluetooth 4.2
GPS/GNSS and barometric altimeter
Built-in speaker and mic
8GB capacity', 50, N'./images/mac/watch3.png', 199.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (74, N'APPLE WATCH SERIES 4', N'40mm or 44mm case; over 30 percent larger display20
Retina LTPO OLED display, 1000 nits
Ion-X glass display on aluminum cases; sapphire crystal display on stainless steel cases
GPS and GPS + Cellular models
S4 SiP with 64-bit dual-core processor; W3 wireless chip
Digital Crown with haptic feedback
Electrical heart sensor and second-generation optical heart sensor
High and low heart rate notifications, irregular heart rhythm notification, and ECG app3
Emergency SOS4 and fall detection
Water resistant 50 meters1
LTE and UMTS,6 Wi-Fi, and Bluetooth 5.0
GPS/GNSS and barometric altimeter
50 percent louder speaker20; built-in mic
16GB capacity', 50, N'./images/mac/watch4.png', 249.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (75, N'APPLE WATCH SERIES 5', N'40mm or 44mm case; over 30 percent larger display20
Always-On Retina LTPO OLED display, 1000 nits
Ion-X glass display on aluminum cases; sapphire crystal display on stainless steel, titanium, and ceramic cases
GPS and GPS + Cellular models
S5 SiP with 64-bit dual-core processor; W3 wireless chip
Digital Crown with haptic feedback
Electrical heart sensor and second-generation optical heart sensor
High and low heart rate notifications, irregular heart rhythm notification, and ECG app3
International emergency calling,5 Emergency SOS,4 and fall detection
Water resistant 50 meters1
LTE and UMTS,6 Wi-Fi, and Bluetooth 5.0
GPS/GNSS, compass, and barometric altimeter
50 percent louder speaker20; built-in mic
32GB capacity', 50, N'./images/mac/watch5.png', 299.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (76, N'APPLE WATCH SERIES 6', N'40mm or 44mm case; over 30 percent larger display20
Always-On Retina LTPO OLED display, 1000 nits
Ion-X glass display on aluminum cases; sapphire crystal display on stainless steel and titanium cases
GPS and GPS + Cellular models
S6 SiP with 64-bit dual-core processor; W3 wireless chip; U1 chip (Ultra Wideband)14
Digital Crown with haptic feedback
Blood oxygen sensor; electrical heart sensor and third-generation optical heart sensor
High and low heart rate notifications, irregular heart rhythm notification, and ECG app3
International emergency calling,5 Emergency SOS,4 and fall detection
Water resistant 50 meters1
LTE and UMTS,6 Wi-Fi, and Bluetooth 5.0
GPS/GNSS, compass, and always-on altimeter
50 percent louder speaker20; built-in mic
32GB capacity', 50, N'./images/mac/watch6.png', 399.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (77, N'APPLE WATCH SERIES 7', N'41mm or 45mm case; over 50 percent more screen area20
Always-On Retina LTPO OLED display, 1000 nits
Ion-X glass display on aluminum cases; sapphire crystal display on stainless steel and titanium cases
GPS and GPS + Cellular models
S7 SiP with 64-bit dual-core processor; W3 wireless chip; U1 chip (Ultra Wideband)14
Digital Crown with haptic feedback
Blood oxygen sensor; electrical heart sensor and third-generation optical heart sensor
High and low heart rate notifications, irregular heart rhythm notification, and ECG app3
International emergency calling,5 Emergency SOS,4 and fall detection
Water resistant 50 meters1
Dustproof (IP6X)
Most crack-resistant front crystal
LTE and UMTS,6 Wi-Fi, and Bluetooth 5.0
GPS/GNSS, compass, and always-on altimeter
50 percent louder speaker20; built-in mic
32GB capacity
Faster charge time', 50, N'./images/mac/watch7.png', 499.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (78, N'APPLE WATCH  SE', N'40mm or 44mm case; over 30 percent larger display20
Retina LTPO OLED display, 1000 nits
Ion-X glass display
GPS and GPS + Cellular models
S5 SiP with 64-bit dual-core processor; W3 wireless chip
Digital Crown with haptic feedback
Second-generation optical heart sensor
High and low heart rate notifications and irregular heart rhythm notification3
International emergency calling,5 Emergency SOS,4 and fall detection
Water resistant 50 meters1
LTE and UMTS,6 Wi-Fi, and Bluetooth 5.0
GPS/GNSS, compass, and always-on altimeter
50 percent louder speaker20; built-in mic
32GB capacity', 50, N'./images/mac/watchse.png', 279.0000, 11, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (79, N'AIRPODS (1st generation)', N'White
H1 chip
“Hey Siri” always on
Up to 5 hours of listening time on one charge3
More than 24 hours of listening time with charging case3
Charging Case
Personalized engraving with initials, emoji, and more', 50, N'./images/mac/airpods.png', 99.0000, 12, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (80, N'AIRPODS (2nd generation)', N'White
H1 chip
“Hey Siri” always on
Up to 5 hours of listening time on one charge3
More than 24 hours of listening time with charging case3
Charging Case
Personalized engraving with initials, emoji, and more', 50, N'./images/mac/airpods.png', 149.0000, 12, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (81, N'AIRPODS (3rd generation)', N'White
Spatial audio with dynamic head tracking1
H1 chip
Sweat and water resistant (IPX4)2
“Hey Siri” always on
Up to 6 hours of listening time on one charge3
Up to 30 hours of listening time with charging case3
MagSafe Charging Case
Personalized engraving with initials, emoji, and more', 50, N'./images/mac/airpods2.png', 179.0000, 12, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (82, N'AORPODS PRO', N'White
Spatial audio with dynamic head tracking1
Active Noise Cancellation
Transparency mode
H1 chip
Sweat and water resistant (IPX4)2
“Hey Siri” always on
Up to 4.5 hours of listening time on one charge3
More than 24 hours of listening time with charging case3
MagSafe Charging Case
Personalized engraving with initials, emoji, and more', 50, N'./images/mac/airpodspro.png', 249.0000, 12, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (83, N'AIRPODS PRO MAX', N'Silver, space gray, sky blue, pink, green
Spatial audio with dynamic head tracking1
Active Noise Cancellation
Transparency mode
H1 chip
“Hey Siri” always on
Up to 20 hours of listening time on one charge3
Smart Case
Personalized engraving with initials, emoji, and more', 50, N'./images/mac/airpodsmax.png', 549.0000, 12, 1)
INSERT [dbo].[Product] ([ProductID], [ProductName], [Description], [Quantity], [image], [Price], [CategoryID], [SellID]) VALUES (85, N'IPHONE SE (3rd gểnation)', N'Midnight, starlight, and (PRODUCT)RED
4.7-inch Retina HD display with True Tone
Glass and aluminum design
Water resistant to a depth of 1 meter for up to 30 minutes (IP67)4
Single 12MP camera (Wide) with Portrait mode, Deep Fusion, Smart HDR 4, Photographic Styles, and 4K video up to 60 fps with extended dynamic range up to 30 fps
7MP FaceTime HD camera with Portrait mode, Deep Fusion, Smart HDR 4, Photographic Styles, and 1080p HD video recording at 30 fps
5G and LTE Advanced2
A15 Bionic chip with new 6-core CPU with 2 performance and 4 efficiency cores, 4-core GPU, and 16-core Neural Engine
Battery life: Up to 15 hours video playback; up to 10 hours video playback (streamed)3
Touch ID for secure authentication and Apple Pay
Qi wireless charging15
Fast-charge capable', 50, N'./images/ip/iphonese3.png', 699.0000, 5, 1)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Created_date]  DEFAULT (getdate()) FOR [Created_date]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD FOREIGN KEY([OrdersID])
REFERENCES [dbo].[Orders] ([OrdersID])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([AdminID])
REFERENCES [dbo].[Account] ([AdminID])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
